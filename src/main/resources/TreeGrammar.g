/*
 * Grammar represents text view of the grammar tree.
 */

root ::= tree;
@skip whitespace {
    tree ::= (value ('(' tree? (',' tree)* ')')?) | emptyvar;
    emptyvar ::= var '(' ')';
}

value ::= exact | var;
exact ::= [A-Za-z]+ | [0-9] | [+-\\*/];
var ::= 'X'[0-9]+;
whitespace ::= [ ]+;

// For possible future implementation:
//value ::= var | terminal | nonterminal;
//terminal ::= [a-z]+;
//nonterminal ::= [A-Z][a-z]*;
