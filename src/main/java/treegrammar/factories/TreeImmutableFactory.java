package treegrammar.factories;

import treegrammar.beans.ITree;
import treegrammar.beans.ImmutableTree;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provide trees with single node. Cache and re-use them to reduce memory consumption.
 */
public class TreeImmutableFactory implements TreeFactory {

    private static Map<String, ITree> leafs = new HashMap<>();

    private static final TreeImmutableFactory instance = new TreeImmutableFactory();

    public static TreeImmutableFactory getInstance() {
        return instance;
    }

    /**
     * Provide tree with single node.
     * @param value Value of the node.
     * @return Tree with single node. Can be cached and reused.
     */
    @Override
    public ITree buildTree(String value) {
        if (leafs.containsKey(value)) {
            return leafs.get(value);
        }

        ITree tree = new ImmutableTree(value, Collections.emptyList());
        leafs.put(value, tree);

        return tree;
    }

    @Override
    public ITree buildTree(String rootValue, List<ITree> newChildren) {
        return new ImmutableTree(rootValue, newChildren);
    }
}
