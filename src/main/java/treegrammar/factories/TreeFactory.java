package treegrammar.factories;

import treegrammar.beans.ITree;

import java.util.List;

/**
 * Created by andremoniy-
 * on 18/06/2017.
 */
public interface TreeFactory {
    ITree buildTree(String value);

    ITree buildTree(String rootValue, List<ITree> newChildren);
}
