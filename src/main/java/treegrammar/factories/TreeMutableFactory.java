package treegrammar.factories;

import treegrammar.beans.ITree;
import treegrammar.beans.MutableTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andremoniy-
 * on 18/06/2017.
 */
public class TreeMutableFactory implements TreeFactory {

    private static TreeFactory instance = new TreeMutableFactory();

    public static TreeFactory getInstance() {
        return instance;
    }

    @Override
    public ITree buildTree(String value) {
        return new MutableTree(value, new ArrayList<>());
    }

    @Override
    public ITree buildTree(String rootValue, List<ITree> newChildren) {
        return new MutableTree(rootValue, new ArrayList<>(newChildren));
    }
}
