package treegrammar.ui.automata;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import treegrammar.automata.AutomataRule;
import treegrammar.automata.Command;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static treegrammar.automata.Command.*;

/**
 * Created by andremoniy -
 * on 18/06/2017.
 */
public class RulesForm {

    private static final Logger log = LoggerFactory.getLogger(RulesForm.class);
    public static final String DEF_FILE = "src/main/resources/deletion_example.txt";
    private JTextPane rulesPane;
    private JPanel panel;
    private JButton rebuildButton;
    private JButton openButton;
    private JButton saveButton;
    private JScrollPane rulesScrollPane;
    private JButton shiftStepsUp;
    private AutomataForm automataForm;
    private String currentFilePath = DEF_FILE;

    private void createUIComponents() {
        rulesPane = new JTextPane();
        loadFile();

        rebuildButton = new JButton("Rebuild");
        rebuildButton.addActionListener(e -> {
            if (automataForm != null) {
                automataForm.restart();
            }
        });

        openButton = new JButton("Open");
        openButton.addActionListener(e -> {
            JFileChooser fc = new JFileChooser();
            fc.setCurrentDirectory(new File(currentFilePath).getParentFile().getAbsoluteFile());
            int rv = fc.showOpenDialog(rulesPane);
            if (rv == JFileChooser.APPROVE_OPTION) {
                File open = fc.getSelectedFile();
                currentFilePath = open.getAbsolutePath();
                loadFile();
            }
        });

        saveButton = new JButton("Save");
        saveButton.addActionListener(e -> {
            try {
                String rulesText = (isNotBlank(automataForm.getTreeFormula().getText()) ? "t:" + automataForm.getTreeFormula().getText() + "\n" : "");
                rulesText += rulesPane.getText();
                FileUtils.writeStringToFile(new File(currentFilePath), rulesText, "utf-8");

                // saving as table
                buildLatexTable();

            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }
        });

        shiftStepsUp = new JButton("States>>");
        shiftStepsUp.addActionListener(e -> {
            List<AutomataRule> rules = parseRules();

            StringBuilder sb = new StringBuilder();
            for (AutomataRule ar : rules) {
                ar.setInitialState(ar.getInitialState() + 1);
                ar.setNextState(ar.getNextState() + 1);
                sb.append(ar.toString()).append("\n");
            }

            rulesPane.setText(sb.toString());
            rulesPane.repaint();
        });
    }

    private void buildLatexTable() throws IOException {
        List<AutomataRule> rules = parseRules();
        int maxState = 0;
        for (AutomataRule ar : rules) {
            if (ar.getInitialState() > maxState) maxState = ar.getInitialState();
            if (ar.getNextState() > maxState && !ar.getSubstituteLabel().equals("#")) maxState = ar.getNextState();
        }

        maxState++;

        Map<String, AutomataRule>[] table = new Map[maxState];
        Set<String> terms = new TreeSet<>();
        for (AutomataRule ar : rules) {
            Map<String, AutomataRule> map = table[ar.getInitialState()];
            if (map == null) {
                map = new TreeMap<>();
                table[ar.getInitialState()] = map;
            }
            String term = ar.getInitialTerm().toString();
            map.put(term, ar);
            terms.add(term);
        }


        StringBuilder sb = new StringBuilder();
        sb.append("\\begin{table}[]\n" +
                "\\centering\n" +
                "\\caption{My caption}\n" +
                "\\label{my-label}\n" +
                "\\tiny\n" +
                "\\setlength{\\tabcolsep}{1pt}\n" +
                "\\begin{tabular}{|" + StringUtils.repeat("l|", terms.size() + 1) + "}\n");

        sb.append("\\hline\n");
        for (String t : terms) {
            sb.append("&").append(t);
        }
        sb.append(" \\\\ \\hline\n");

        for (int i = 0; i < table.length; i++) {
            sb.append(i);

            Map<String, AutomataRule> map = table[i];
            if (map == null) {
                sb.append(StringUtils.repeat("&", terms.size() - 1));
            } else {
                for (String t : terms) {
                    sb.append("&");
                    AutomataRule ar = map.get(t);
                    if (ar != null) {
                        if (ar.getSubstituteLabel().equals("#")) sb.append("\\#");
                        else {
                            sb.append("\\pbox{20cm}{");
                            sb.append(ar.getNextState()).append(",").append(ar.getSubstituteLabel()).append("\\\\");
                            switch (ar.getCommand()) {
                                case UP: {
                                    sb.append("$\\uparrow$");
                                    break;
                                }
                                case DOWN: {
                                    sb.append("$\\downarrow$");
                                    break;
                                }
                                case LEFT: {
                                    sb.append("$\\leftarrow$");
                                    break;
                                }
                                case RIGHT: {
                                    sb.append("$\\rightarrow$");
                                    break;
                                }
                            }
                            sb.append("}");
                        }
                    } else sb.append("");
                }
            }
            sb.append(" \\\\ \\hline\n");
        }
        sb.append("\\end{tabular}\n" +
                "\\end{table}\n");

        FileUtils.writeStringToFile(new File(currentFilePath + ".tex"), sb.toString(), "utf-8");
    }

    private List<AutomataRule> parseRules() {
        String[] rulesArr = rulesPane.getText().split("\n");
        List<String> rulesList = new ArrayList<>();
        Collections.addAll(rulesList, rulesArr);
        return AutomataRule.parseRules(rulesList);
    }

    private void loadFile() {
        try {
            List<String> rulesText = FileUtils.readLines(new File(currentFilePath), "utf-8");
            List<String> trees = new ArrayList<>();
            for (String r : rulesText) {
                if (r.startsWith("t:")) trees.add(r.substring(2));
                else break;
            }
            String rules = "";
            for (int i = trees.size(); i < rulesText.size(); i++) {
                String rt = rulesText.get(i);
                rules += rt + "\n";
            }
            if (!trees.isEmpty()) {
                automataForm.getTreeFormula().setText(trees.get(0));
            }
            rulesPane.setText(rules);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public JTextPane getRulesPane() {
        return rulesPane;
    }

    public Container getPanel() {
        return panel;
    }

    public void setAutomataForm(AutomataForm automataForm) {
        this.automataForm = automataForm;
    }
}
