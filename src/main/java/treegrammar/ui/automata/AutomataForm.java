package treegrammar.ui.automata;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import treegrammar.automata.Automata;
import treegrammar.automata.AutomataRule;
import treegrammar.factories.TreeMutableFactory;
import treegrammar.grammar.GrammarParser;
import treegrammar.beans.ITree;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Created by andremoniy -
 * on 18/06/2017.
 */
public class AutomataForm {
    private final RulesForm rulesForm;
    private JButton nextStepButton;
    private JPanel panel;
    private JTextPane logPane;
    private JScrollPane logScrollPane;
    private JTextArea treeFormula;
    private JButton a10StepsButton;
    private Automata automata;
    private static mxGraphComponent lastGraphComponent;

    private void drawTree(Container contentPane, ITree activeNode, ITree activeParent) {
        ITree tree = automata.getTree();
        mxGraph graph = new mxGraph();
        Object parent = graph.getDefaultParent();

        graph.getModel().beginUpdate();
        try {
            Object node = getNode(tree, graph, parent, tree == activeParent ? "fillColor=yellow" : null);

            recurTree(graph, parent, tree, node, activeNode, activeParent);

        } finally {
            graph.getModel().endUpdate();
        }

        mxHierarchicalLayout mhl = new mxHierarchicalLayout(graph);
        mhl.execute(parent);

        if (lastGraphComponent != null) contentPane.remove(lastGraphComponent);
        lastGraphComponent = new mxGraphComponent(graph);
        contentPane.add(lastGraphComponent);
    }

    private static Object getNode(ITree tree, mxGraph graph, Object parent, String style) {
        return graph.insertVertex(parent, null, tree.label(), 0, 0, 20, 20, style);
    }

    private static void recurTree(mxGraph graph, Object parent, ITree tn, Object node, ITree activeNode, ITree activeParent) {
        for (ITree cn : tn.getChildren()) {
            Object cnode = getNode(cn, graph, parent, cn == activeNode ? "fillColor=green" : (cn == activeParent ? "fillColor=yellow" : null));
            graph.insertEdge(parent, null, null, node, cnode);
            recurTree(graph, parent, cn, cnode, activeNode, activeParent);
        }
    }

    public AutomataForm(RulesForm rulesForm) {
        this.rulesForm = rulesForm;
        init(getiTree());
    }

    private void init(ITree tree) {

        String[] rulesArr = rulesForm.getRulesPane().getText().split("\n");
        List<String> rulesList = new ArrayList<>();
        Collections.addAll(rulesList, rulesArr);
        List<AutomataRule> rules = AutomataRule.parseRules(rulesList);

        automata = Automata.getAutomata();
        automata.init(tree, rules);
    }

    public void restart() {
        ITree tree = getiTree();
        init(tree);
        drawTree(panel, automata.getCurrentNode(), automata.getCurrentParent());
        logPane.setText("");
        logPane.revalidate();
        panel.revalidate();
    }

    public ITree getiTree() {
        return new GrammarParser(TreeMutableFactory.getInstance()).parseTree(treeFormula.getText());
//        return new GrammarParser(TreeMutableFactory.getInstance()).parseTree("S(a, t(b, b, t(b, b, t(a, b, b, b, b, a, b, b), b, b, a, b), b, b), b)");
//        return new GrammarParser(TreeMutableFactory.getInstance()).parseTree("S(a,t(b,b,t(b,a,b,b,b,a),b,b),b)");
//        return new GrammarParser(TreeMutableFactory.getInstance()).parseTree("S(a,t(b,b,a,b), b)");
//        L(a(l(x,p(x,a)),a(l(y,p(y,b)),c)))
    }

    public static void main(String[] args) throws IOException {
        RulesForm rulesForm = new RulesForm();

        AutomataForm form = new AutomataForm(rulesForm);

        {
            JFrame frame = new JFrame("Automata form");
            frame.setContentPane(form.panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            form.drawTree(form.panel, form.automata.getCurrentNode(), form.automata.getCurrentParent());

            frame.pack();
            frame.setVisible(true);
        }
        {
            JFrame frame = new JFrame("Rules form");
            frame.setContentPane(rulesForm.getPanel());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
            rulesForm.setAutomataForm(form);
        }
    }

    private void createUIComponents() {
        nextStepButton = new JButton("Next step");
        nextStepButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                automata.nextStep();
                logPane.setText(logPane.getText() + "\n" + automata.getLastRule());
                drawTree(panel, automata.getCurrentNode(), automata.getCurrentParent());
                panel.revalidate();
            }
        });

        a10StepsButton = new JButton("10 steps");
        a10StepsButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < 10; i++) {
                    automata.nextStep();
                    logPane.setText(logPane.getText() + "\n" + automata.getLastRule());
                }
                drawTree(panel, automata.getCurrentNode(), automata.getCurrentParent());
                panel.revalidate();
            }
        });
    }

    public JTextArea getTreeFormula() {
        return treeFormula;
    }
}
