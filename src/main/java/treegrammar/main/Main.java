package treegrammar.main;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.nio.ch.IOUtil;
import treegrammar.grammar.GrammarParser;
import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;
import treegrammar.factories.TreeImmutableFactory;
import treegrammar.producer.Producer;
import treegrammar.search.SearchBFS;
import treegrammar.search.SearchResult;
import treegrammar.search.TreeSearch;
import treegrammar.util.GrammarFileUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Scanner;

/**
 * Main class to launch this application.
 */
public class Main {
    // usage parameters
    private static final String USAGE = "<path-to-file> [-m left|right|strict] " +
            "[-i <iteration limit>] [-rl <recursion limit>]";

    // demo list of rules
    private static final String DEMO_LIST = "/gr1.txt";

    // initial tree
    private static final ITree INITIAL_TREE = TreeImmutableFactory.getInstance().buildTree("S");

    // logger
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private static List<String> getDemoList() throws IOException {
        return IOUtils.readLines(Main.class.getResourceAsStream(DEMO_LIST), "utf-8");
    }

    /**
     * Run command line interface for this program.
     * @param args Path to file with rules,
     *             number of rules in a row, number of same rules in a row.
     */
    private static void runCLI(String... args) {
        GrammarParser parser = GrammarParser.getParser();
        List<GrammarRule> rules;
        Producer.Mode mode = Producer.Mode.LEFT;
        int iterationLimit = 50;
        int recursionLimit = 4;

        Queue<String> arguments = new LinkedList<>(Arrays.asList(args));
        try {
            if (arguments.isEmpty()) {
                LOG.info("Starting in DEMO mode.");
                LOG.info("Normal usage: " + USAGE);
                rules = parser.parseRules(getDemoList());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    // do nothing
                }
                LOG.info("Rules list:");
                for (GrammarRule rule : rules) {
                    LOG.info(rule.toString());
                }
                LOG.info("Parser type: LEFT oriented");
                LOG.info("Maximum number of rules in a row (iteration limit): 50");
                LOG.info("Appliance of the same rule in a row limit (recursion limit): 4");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    // do nothing
                }

            } else {
                rules = parser.parseRules(GrammarFileUtils.readSource(arguments.remove()));
            }

            while (!arguments.isEmpty()) {
                String flag = arguments.remove();
                try {
                    switch (flag) {
                        case "-m":
                            String stringMode = arguments.remove();
                            mode = Producer.Mode.valueOf(stringMode.toUpperCase());
                            break;
                        case "-i":
                            iterationLimit = Integer.parseInt(arguments.remove());
                            if (iterationLimit <= 0) {
                                LOG.error("Iteration limit should be positive.");
                                return;
                            }
                            break;
                        case "-rl":
                            recursionLimit = Integer.parseInt(arguments.remove());
                            if (recursionLimit <= 0) {
                                LOG.error("Recursion limit should be positive.");
                                return;
                            }
                            break;
                        default:
                            LOG.error("Unknown option: \"" + flag + "\"");
                            throw new IllegalArgumentException();
                    }
                } catch (NoSuchElementException nsee) {
                    LOG.error("Missing argument for " + flag);
                    return;
                } catch (NumberFormatException nfe) {
                    LOG.error("Unable to parse number for " + flag);
                    return;
                }
            }
        } catch (IllegalArgumentException iae) {
            LOG.error("Usage: " + USAGE);
            return;
        } catch (IOException ioe) {
            LOG.error(ioe.getMessage());
            LOG.error("Usage: " + USAGE);
            return;
        }

        Producer producer = Producer.getDefaultProducer(mode);
        TreeSearch searcher = SearchBFS.getSearchBFS(iterationLimit, recursionLimit);
        searcher.startSearch(INITIAL_TREE, rules, producer);
        LOG.info("----------- SEARCH STARTED --------------");

        byte progress = -1;
        while (!searcher.isDone()) {
            try {
                Thread.sleep(3000);
                byte currentProgress = searcher.getProgress();
                if (currentProgress > progress) {
                    progress = currentProgress;
                    LOG.info("Current progress: " + progress + "%.");
                }
            } catch (InterruptedException e) {
                break;
            }
        }

        List<SearchResult> results = searcher.getResult();

        LOG.info("----------- SEARCH FINISHED --------------");
        LOG.info(results.size() + " trees was found: ");
        for (SearchResult result : results) {
            LOG.info(result.getResultTree().toString());
            TreeSearch.showResult(result, producer);
            TreeSearch.showRuleStat(result);
        }
        TreeSearch.showTotalStat(rules, results);
    }

    /**
     * Starting point of this app.
     * @param args <path-to-file> [-m LEFT|RIGHT|STRICT] [-i iteration limit] [-rl recursion limit]
     */
    public static void main(String... args) {
        runCLI(args);
    }
}
