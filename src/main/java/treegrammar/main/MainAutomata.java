package treegrammar.main;

import treegrammar.automata.Automata;
import treegrammar.automata.AutomataRule;
import treegrammar.grammar.GrammarParser;
import treegrammar.beans.ITree;
import treegrammar.util.GrammarFileUtils;

import java.io.IOException;
import java.util.List;

/**
 * Main class for automata part of the program.
 */
public class MainAutomata {

    public static void main(String... args) throws IOException {
//        List<String> rulesText = Arrays.asList(
//                "0, F(S) -> 1, S, R",
//                "1, F() -> 1, S, D",
//                "1, S() -> 2, a, U",
//                "2, F(S) -> 3, S, L",
//                "3, F(S) -> 3, -, R");

        List<AutomataRule> rules = readRules();

        ITree tree = GrammarParser.getParser().parseTree("S(a,t(b,b,a,b), b)");
        System.out.println(tree);
        System.out.println();

        Automata.getAutomata().process(tree, rules);
    }

    public static List<AutomataRule> readRules() throws IOException {
        List<String> rulesText = GrammarFileUtils.readSource("automata_rules.txt");

        List<AutomataRule> rules = AutomataRule.parseRules(rulesText);
        for (AutomataRule rule : rules) {
            System.out.println(rule);
        }
        System.out.println();
        return rules;
    }
}
