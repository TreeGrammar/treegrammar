package treegrammar.grammar;

import treegrammar.beans.ITree;

import java.util.List;

/**
 * Immutable tree for indexes of the pattern in the initial tree.
 */
public class IndexTree {
    private static final IndexTree EMPTY_INDEX_TREE = new IndexTree(0);
    private final int value;
    private final IndexTree[] children;

    private int hash = 0;

    /**
     * Construct node of index tree.
     * @param value Index of the child node.
     * @param children Zero or more sub-trees. Order is matter and will be preserved.
     */
    public IndexTree(int value, IndexTree... children) {
        this.value = value;
        this.children = children.clone();
    }

    public static IndexTree getEmptyTree() {
        return EMPTY_INDEX_TREE;
    }

    public static IndexTree buildIndexTree(ITree tree) {
        if (!tree.hasChildren()) { return EMPTY_INDEX_TREE; }
        int value = 0;

        List<ITree> rootChildren = tree.getChildren();
        IndexTree[] newChildren = new IndexTree[rootChildren.size()];
        for (int i = 0; i < rootChildren.size(); i++) {
            ITree child = rootChildren.get(i);
            newChildren[i] = buildIndexTree(child);
        }

        return new IndexTree(value, newChildren);
    }

    public int getRootValue() {
        return value;
    }

    private boolean hasChildren() {
        return children.length != 0;
    }

    public int getChildrenSize() {
        return children.length;
    }

    public IndexTree getChild(int i) {
        return children[i];
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(value);

        if (hasChildren()) {
            result.append('(');
            for (IndexTree child : children) {
                result.append(child).append(", ");
            }
            result.delete(result.length() - 2, result.length());
            result.append(')');
        }

        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || !(o instanceof IndexTree)) { return false; }

        IndexTree that = (IndexTree) o;

        if (value != that.getRootValue()) { return false; }

        if (children.length != that.children.length) { return false; }

        for (int i = 0; i < children.length; i++) {
            if (children[i] != that.children[i]) { return false; }
        }

        return true;

    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            final int prime = 31;
            int result = value;

            for (IndexTree child : children) {
                result = prime * result + child.hashCode();
            }

            hash = result;
        }

        return hash;
    }

//    @Override
//    public Iterator iterator() {
//        return new Iterator() {
//            private int pos = 0;
//
//            @Override
//            public boolean hasNext() {
//                return pos < children.length;
//            }
//
//            @Override
//            public Object next() {
//                return children[pos++];
//            }
//        };
//    }
}
