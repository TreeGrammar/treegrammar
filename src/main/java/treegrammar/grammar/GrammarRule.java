package treegrammar.grammar;

import treegrammar.beans.ITree;

/**
 * Represents immutable grammar tree transformation rule of following type:
 * TreeGrammar -> TreeGrammar .
 */
public class GrammarRule implements Comparable<GrammarRule> {

    private final ITree pattern;
    private final ITree transformation;
    private final int ruleNum;

    /**
     * Should be instantiated through rule parser which check correctness of the rule.
     * @param pattern TreeGrammar represented pattern to find.
     * @param transformation TreeGrammar represented transformation to make.
     */
    GrammarRule(ITree pattern, ITree transformation, int ruleNum) {
        this.pattern = pattern;
        this.transformation = transformation;
        this.ruleNum = ruleNum;
    }

    public ITree getPattern() {
        return pattern;
    }

    public ITree getTransformation() {
        return transformation;
    }

    public int getRuleNum() {
        return ruleNum;
    }

    @Override
    public String toString() {
        return pattern.toString() + " -> " + transformation.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GrammarRule that = (GrammarRule) o;

        return pattern.equals(that.pattern) && transformation.equals(that.transformation);

    }

    @Override
    public int hashCode() {
        int result = pattern.hashCode();
        result = 31 * result + transformation.hashCode();
        return result;
    }

    @Override
    public int compareTo(GrammarRule that) {
        return Integer.compare(this.ruleNum, that.ruleNum);
    }
}
