package treegrammar.grammar;

import lib6005.parser.GrammarCompiler;
import lib6005.parser.ParseTree;
import lib6005.parser.Parser;
import lib6005.parser.UnableToParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import treegrammar.beans.ITree;
import treegrammar.factories.TreeFactory;
import treegrammar.factories.TreeImmutableFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class which parseRules rules from different sources.
 */
public class GrammarParser {
    // File with grammar describing grammar tree
    private static final String GRAMMAR_FILE = "TreeGrammar.g";

    // logger
    private static final Logger LOG = LoggerFactory.getLogger(GrammarParser.class);

    private final TreeFactory treeFactory;

    // Singleton
    private static GrammarParser parser = new GrammarParser(new TreeImmutableFactory());

    public GrammarParser(TreeFactory treeFactory) {
        this.treeFactory = treeFactory;
    }

    /**
     * Fabric of this class.
     *
     * @return Instance of GrammarParser.
     */
    public static GrammarParser getParser() {
        return parser;
    }

    private enum TreeGrammarNodeType {ROOT, TREE, VALUE, WHITESPACE, EMPTYVAR, EXACT, VAR}

    /**
     * Parse tree.
     *
     * @param input String with tree defined as tree ::= value ('(' tree (',' tree)* ')')?
     * @return Abstract tree as TreeGrammar.
     * @throws IllegalArgumentException if the input doesn't match conditions.
     */
    public ITree parseTree(String input) {
        Parser<TreeGrammarNodeType> fileParser;

        try {
            InputStream stream = this.getClass().getClassLoader().getResourceAsStream(GRAMMAR_FILE);
            if (stream == null) {
                throw new IOException();
            }
            fileParser = GrammarCompiler.compile(stream, TreeGrammarNodeType.ROOT);
            ParseTree<TreeGrammarNodeType> tree = fileParser.parse(input);

            return parseTree(tree);
        } catch (UnableToParseException e) {
            LOG.error("Input string doesn't match TreeGrammar grammar.", e);

            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            LOG.error("Unable to open " + GRAMMAR_FILE);

            throw new RuntimeException(e);
        }
    }

    private ITree parseTree(ParseTree<TreeGrammarNodeType> tree) {
        switch (tree.getName()) {
            case ROOT:
                return parseTree(tree.childrenByName(TreeGrammarNodeType.TREE).get(0));
            case TREE:
                // empty variable
                if (!tree.childrenByName(TreeGrammarNodeType.EMPTYVAR).isEmpty()) {
                    ParseTree<TreeGrammarNodeType> node = tree.childrenByName(TreeGrammarNodeType.EMPTYVAR).get(0);
                    String nodeValue = node.childrenByName(TreeGrammarNodeType.VAR).get(0).getContents();
                    return ITree.buildVar(nodeValue, false);
                }

                ParseTree<TreeGrammarNodeType> node = tree.childrenByName(TreeGrammarNodeType.VALUE).get(0);
                TreeGrammarNodeType type;
                if (node.childrenByName(TreeGrammarNodeType.VAR).isEmpty()) {
                    type = TreeGrammarNodeType.EXACT;
                } else {
                    type = TreeGrammarNodeType.VAR;
                }

                String value = tree.childrenByName(TreeGrammarNodeType.VALUE).get(0).getContents();

                // no actual children
                if (tree.childrenByName(TreeGrammarNodeType.TREE).isEmpty()) {
                    if (type == TreeGrammarNodeType.EXACT) {
                        return treeFactory.buildTree(value);
                    } else {
                        // for variable it actually means 'any children'
                        return ITree.buildVar(value, true);
                    }
                }

                List<ITree> children = tree.childrenByName(
                        TreeGrammarNodeType.TREE).stream().map(this::parseTree).collect(Collectors.toList());

                if (type == TreeGrammarNodeType.EXACT) {
                    return treeFactory.buildTree(value, children);
                } else {
                    return ITree.buildVar(value, children);
                }

                // we are always avoiding to call parseTree() with this types:
            case VALUE:
            case WHITESPACE:
        }

        String errorMessage = "Reached normally unreachable part of code.";
        LOG.error(errorMessage);
        throw new RuntimeException(errorMessage);
    }

    /**
     * Parse list of text rules.
     *
     * @param textRules One or more grammar tree transformation rules.
     * @return List of GrammarRules.
     * @throws IllegalArgumentException if any rule doesn't match pattern.
     */
    public List<GrammarRule> parseRules(Iterable<String> textRules) {
        List<GrammarRule> rules = new ArrayList<>();

        try {
            int ruleNum = 1;
            for (String textRule : textRules) {
                String[] parts = textRule.split("\\s*->\\s*");
                ITree pattern = parseTree(parts[0]);
                ITree transformation = parseTree(parts[1]);

                rules.add(new GrammarRule(pattern, transformation, ruleNum++));
            }
        } catch (Exception e) {
            LOG.error("Unable to parse rules.", e);
            throw new RuntimeException(e);
        }

        return rules;
    }

    public List<GrammarRule> parseRules(String... textRules) {
        return parseRules(Arrays.asList(textRules));
    }
}
