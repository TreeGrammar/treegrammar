package treegrammar.matcher;

import treegrammar.beans.ITree;
import treegrammar.factories.TreeImmutableFactory;
import treegrammar.grammar.IndexTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Match tree with exact pattern.
 *
 * LEFT_BORDER:
 * Match tree with pattern iff tree root corresponds to pattern root and
 * for each corresponding node each list of children from pattern
 * are right most sublist of children from tree. E.g.:
 *      Pattern: X1(a,b)
 *      Matched trees: A(a,b); A(a, a, b); B(a, b, c, a, b)
 *      Not matched trees: A(a, b, c) ('a, b' are not on right side)
 *                         A(a, c, b) ('a, b' has 'c' between)
 *
 * RIGHT_BORDER:
 * Match tree with pattern iff tree root corresponds to pattern root and
 * for each corresponding node each list of children from pattern
 * are left most sublist of children from tree. E.g.:
 *      Pattern: X1(a,b)
 *      Matched trees: A(a,b); A(a, b, c); B(a, b, c, a, b)
 *      Not matched trees: A(c, a, b) ('a, b' are not on the left side)
 *                         A(a, c, b) ('a, b' has 'c' between)
 *
 * STRICT:
 * Match tree with pattern iff tree root corresponds to pattern root,
 * size of the tree equals to size of the pattern and
 * each pattern node strictly corresponds to tree node in terms of order. E.g.:
 *      Pattern: X1(a,b)
 *      Matched trees: A(a,b)
 *      Not matched trees: A(c, a, b) (different tree size)
 *                         A(b, a) (wrong order of nodes)
 *
 */
class MatcherExact implements Matcher {
    enum Mode { LEFT_BORDER, RIGHT_BORDER, STRICT }

    @Override
    public MatcherResult match(ITree tree, ITree pattern) {
        return match(tree, pattern, Mode.STRICT);
    }

    /**
     * Match tree with pattern according to chosen mode.
     * @param tree TreeGrammar to match.
     * @param pattern TreeGrammar pattern from GrammarRule.
     * @param mode Chosen mode to match tree.
     * @return MatcherResult if tree is matched or null if tree is not matched.
     */
    MatcherResult match(ITree tree, ITree pattern, Mode mode) {
        // root value match
        if (!matchRootValue(tree, pattern)) { return null; }
        Map<String, ITree> variables = getRootVar(tree, pattern);

        // children match
        // variable points to whole tree
        if (pattern.isRootVar() && pattern.varHasAnyChildren()) {
            return new MatcherResult(tree, variables, IndexTree.buildIndexTree(tree));
        }

        // no children
        if (!pattern.hasChildren()) {
            if (tree.hasChildren()) { return null; }
            return new MatcherResult(tree, variables, IndexTree.getEmptyTree());
        }

        if (tree.getChildren().size() < pattern.getChildren().size()) {
            return null;
        }

        IndexTree[] childrenIndex = new IndexTree[pattern.getChildren().size()];
        List<ITree> treeChildren = tree.getChildren();
        List<ITree> patternChildren = pattern.getChildren();

        boolean isMatched = true;
        int startPos = 0;
        int lastPos = treeChildren.size();
        int shift = 0;
        switch (mode) {
            case STRICT:
                if (treeChildren.size() != patternChildren.size()) {
                    isMatched = false;
                }
                break;
            case LEFT_BORDER:
                if (treeChildren.size() > patternChildren.size()) {
                    shift = treeChildren.size() - patternChildren.size();
                }
                MatcherResult leftChild =
                        match(treeChildren.get(shift), patternChildren.get(0), Mode.LEFT_BORDER);
                if (leftChild == null) {
                    isMatched = false;
                } else {
                    childrenIndex[0] = leftChild.getPatternIndex();
                    variables.putAll(leftChild.getVariables());
                }
                startPos = shift + 1;
                break;
            case RIGHT_BORDER:
                lastPos = patternChildren.size() - 1;
                MatcherResult rightChild =
                        match(treeChildren.get(lastPos), patternChildren.get(lastPos), Mode.RIGHT_BORDER);
                if (rightChild == null) {
                    isMatched = false;
                } else {
                    childrenIndex[lastPos] = rightChild.getPatternIndex();
                    variables.putAll(rightChild.getVariables());
                }
                break;
        }

        if (!isMatched) { return null; }

        for (int i = startPos; i < lastPos; i++) {
            MatcherResult childResult =
                    match(treeChildren.get(i), patternChildren.get(i - shift), Mode.STRICT);
            if (childResult == null) {
                isMatched = false;
                break;
            }

            childrenIndex[i - shift] = childResult.getPatternIndex();
            variables.putAll(childResult.getVariables());
        }

        if (!isMatched) { return null; }

        return new MatcherResult(tree, variables, new IndexTree(shift, childrenIndex));
    }

    /**
     * Check if root values are equal. If pattern root value is a variable, values also considered equal.
     * @param tree Tree with root value to check.
     * @param pattern Pattern tree with root value to compare.
     * @return true if values are equal or pattern root value it a variable.
     */
    static boolean matchRootValue(ITree tree, ITree pattern) {
        return pattern.isRootVar() || tree.getRootValue().equals(pattern.getRootValue());
    }

    static Map<String, ITree> getRootVar(ITree tree, ITree pattern) {
        Map<String, ITree> variables = new HashMap<>();
        if (pattern.isRootVar()) {
            if (pattern.varHasAnyChildren()) {
                variables.put(pattern.getRootValue(), tree);
            } else {
                variables.put(pattern.getRootValue(), TreeImmutableFactory.getInstance().buildTree(tree.getRootValue()));
            }
        }
        return variables;
    }
}
