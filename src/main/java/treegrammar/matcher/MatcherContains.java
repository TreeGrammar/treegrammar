package treegrammar.matcher;

import treegrammar.beans.ITree;
import treegrammar.grammar.IndexTree;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Match tree with pattern iff amy sub-tree matches pattern. E.g.:
 *      Pattern: X1(a,b)
 *      Matched trees: A(a,b); A(a, b, c); B(a, b, c, a, b)
 *      Not matched trees: A(a, c, b) ('a, b' has 'c' between)
 */
class MatcherContains implements Matcher {

    @Override
    public MatcherResult match(ITree tree, ITree pattern) {
        // check match on this level
        MatcherResult match = isMatchedOnThisLevel(tree, pattern);

        // check sub-trees if not successful
        if (match == null) {
            for (ITree child : tree.getChildren()) {
                match = match(child, pattern);
                if (match != null) {
                    break;
                }
            }
        }

        return match;
    }

    /**
     * Check if tree match pattern on this level. E.g. pattern root corresponds to tree root.
     * @param tree Tree to match.
     * @param pattern Pattern tree for matching.
     * @return true if tree matches pattern.
     */
    private MatcherResult isMatchedOnThisLevel(ITree tree, ITree pattern) {
        // root value match
        if (!MatcherExact.matchRootValue(tree, pattern)) { return null; }

        Map<String, ITree> variables = MatcherExact.getRootVar(tree, pattern);

        // Several blocks according to number of children in pattern.
        // variable points to whole tree
        if (pattern.isRootVar() && pattern.varHasAnyChildren()) {
            return new MatcherResult(tree, variables, IndexTree.buildIndexTree(tree));
        }

        // No children
        if (!pattern.hasChildren()) {
            if (tree.hasChildren()) { return null; }
            return new MatcherResult(tree, variables, IndexTree.getEmptyTree());
        }

        if (tree.getChildren().size() < pattern.getChildren().size()) {
            return null;
        }

        IndexTree[] childrenIndex = new IndexTree[pattern.getChildren().size()];
        List<ITree> treeChildren = tree.getChildren();
        List<ITree> patternChildren = pattern.getChildren();

        // One child
        if (patternChildren.size() == 1) {
            for (int i = 0; i < treeChildren.size(); i++) {
                ITree child = treeChildren.get(i);
                if (MatcherExact.matchRootValue(child, patternChildren.get(0))) {
                    MatcherResult childResult = match(child, patternChildren.get(0));

                    if (childResult != null) {
                        variables.putAll(childResult.getVariables());
                        childrenIndex[0] = childResult.getPatternIndex();
                        IndexTree patternIndex = new IndexTree(i, childrenIndex);

                        return new MatcherResult(tree, variables, patternIndex);
                    }
                }
            }
            return null;
        }

        // More than one child
        int shift;
        for (shift = 0; shift <= treeChildren.size() - patternChildren.size(); shift++) {
            boolean isFound = true;
            for (int i = shift; i < shift + patternChildren.size(); i++) {
                if (!MatcherExact.matchRootValue(treeChildren.get(i), patternChildren.get(i - shift))) {
                    isFound = false;
                    break;
                }
            }
            if (isFound) {
                Map<String, ITree> childrenVars = new HashMap<>();

                MatcherExact matcher = new MatcherExact();
                MatcherExact.Mode mode;

                int lastPos = shift + patternChildren.size() - 1;
                for (int i = shift; i <= lastPos; i++) {
                    if (i == shift) { mode = MatcherExact.Mode.LEFT_BORDER; }
                    else if (i == lastPos) { mode = MatcherExact.Mode.RIGHT_BORDER; }
                    else { mode = MatcherExact.Mode.STRICT; }

                    MatcherResult childResult = matcher.match(
                            treeChildren.get(i), patternChildren.get(i - shift), mode);

                    if (childResult == null) {
                        isFound = false;
                        break;
                    }

                    childrenIndex[i - shift] = childResult.getPatternIndex();
                    childrenVars.putAll(childResult.getVariables());
                }

                if (isFound) {
                    variables.putAll(childrenVars);
                    IndexTree patternIndex = new IndexTree(shift, childrenIndex);
                    return new MatcherResult(tree, variables, patternIndex);
                }
            }
        }

        return null;
    }


}
