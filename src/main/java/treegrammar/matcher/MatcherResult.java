package treegrammar.matcher;

import treegrammar.beans.ITree;
import treegrammar.grammar.IndexTree;

import java.util.Map;

/**
 * Represents the result of the match() operation.
 * Contains root of the matched sub-tree, variable values and
 * indexes of matched by pattern nodes.
 */
public class MatcherResult {
    private final ITree match;
    private final Map<String, ITree> variables;
    private final IndexTree patternIndex;

    MatcherResult(ITree match, Map<String, ITree> variables, IndexTree patternIndex) {
        this.match = match;
        this.variables = variables;
        this.patternIndex = patternIndex;
    }

    public ITree getMatch() {
        return match;
    }

    public Map<String, ITree> getVariables() {
        return variables;
    }

    public IndexTree getPatternIndex() {
        return patternIndex;
    }

    @Override
    public String toString() {
        return "MatcherResult: {" +
                "match: " + match +
                ", variables: " + variables +
                ", patternIndex: " + patternIndex +
                '}';
    }

    // equals and hashCode are implemented for testing purpose
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatcherResult that = (MatcherResult) o;

        return match.equals(that.match) &&
               variables.equals(that.variables) &&
               patternIndex.equals(that.patternIndex);

    }

    @Override
    public int hashCode() {
        int result = match.hashCode();
        result = 31 * result + variables.hashCode();
        result = 31 * result + patternIndex.hashCode();
        return result;
    }
}
