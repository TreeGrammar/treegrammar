package treegrammar.matcher;

import treegrammar.beans.ITree;

/**
 * Match tree with the pattern.
 */
public interface Matcher {
    /**
     * Provide default matcher.
     * @return Matcher which is currently considered default.
     */
    static Matcher getDefaultMatcher() {
        return new MatcherContains();
    }

    /**
     * Find sub-tree which matches the pattern.
     * @param tree TreeGrammar to match.
     * @param pattern TreeGrammar with pattern. Can contain variables.
     * @return Root of the matched sub-tree or null if tree doesn't match the tree.
     */
    MatcherResult match(ITree tree, ITree pattern);
}
