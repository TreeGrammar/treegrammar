package treegrammar.automata;

import treegrammar.beans.ITree;

import java.util.Collections;
import java.util.List;

/**
 * This stub is used when automata goes out of borders of the tree.
 */
class ZeroNode implements ITree {
    @Override
    public String toString() {
        return "-";
    }

    @Override
    public String label() {
        return "-";
    }

    @Override
    public String getRootValue() {
        return "-";
    }

    @Override
    public boolean hasChildren() {
        return false;
    }

    @Override
    public List<ITree> getChildren() {
        return Collections.emptyList();
    }

    @Override
    public boolean isRootVar() {
        return false;
    }

    @Override
    public boolean varHasAnyChildren() {
        return false;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }
}
