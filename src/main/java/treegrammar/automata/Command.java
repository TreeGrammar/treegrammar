package treegrammar.automata;

/**
 * Enum represents commands of automata
 */
public enum Command {
    UP("U"), DOWN("D"), LEFT("L"), RIGHT("R");

    private String text;

    Command(String text) {
        this.text = text;
    }

    public static Command fromString(String text) {
        for (Command command : Command.values()) {
            if (command.text.equalsIgnoreCase(text)) {
                return command;
            }
        }

        throw new IllegalArgumentException("Text doesn't match any command");
    }

    public String getText() {
        return text;
    }
}
