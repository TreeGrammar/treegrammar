package treegrammar.automata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import treegrammar.beans.ITree;
import treegrammar.factories.TreeFactory;
import treegrammar.factories.TreeMutableFactory;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of automata.
 */
class AutomataImpl implements Automata {
    private static final ITree ZERO_NODE = new ZeroNode();

    private Deque<ITree> parentsStack = new ArrayDeque<>();
    private Deque<Integer> parentsStackPos = new ArrayDeque<>();

    private ITree currentParent;
    private ITree currentNode;
    private int childrenPos;
    private int currentState;

    private static final TreeFactory treeFactory = TreeMutableFactory.getInstance();

    private Map<Integer, List<AutomataRule>> rulesByState;

    private static final Logger LOG = LoggerFactory.getLogger(AutomataImpl.class.getSimpleName());
    private AutomataRule lastRule;

    AutomataImpl() {}

    @Override
    public void process(ITree tree, List<AutomataRule> rules) {
        init(tree, rules);
        LOG.debug("----------------- Begin process -----------------------------");

        while (true) {
            if (nextStep()) return;
        }
    }

    @Override
    public void init(ITree tree, List<AutomataRule> rules) {
        rulesByState = buildStateMap(rules);

        currentParent = treeFactory.buildTree("F", Collections.singletonList(tree));
        currentNode = tree;
        currentState = 0;
        childrenPos = 0;

        logAutomataStatus();
    }

    @Override
    public boolean nextStep() {
        lastRule = getNextRule();
        if (lastRule == null) {
            return true;
        }

        if (lastRule.getSubstituteLabel().equals("#")) {
            reportFinish("Finished final rule.");
            return true;
        }

        if (lastRule.getSubstituteLabel().equals("-")) {
            deleteCurrentNode(lastRule.getCommand());
        } else {
            // write new value only if it's different
            if (!lastRule.getSubstituteLabel().equals(currentNode.getRootValue())) {
                writeThisNode(lastRule.getSubstituteLabel());
            } else {
                LOG.debug("No write operation needed");
            }

            moveCursor(lastRule.getCommand());
        }

        currentState = lastRule.getNextState();
        LOG.debug("----------------- Rule applied -----------------------------");
        return false;
    }

    private AutomataRule getNextRule() {
        List<AutomataRule> rulesForCurState = rulesByState.get(currentState);

        if (rulesForCurState == null || rulesForCurState.isEmpty()) {
            reportFinish("No rules for this state");
            return null;
        }

        for (AutomataRule rule : rulesForCurState) {
            Term term = rule.getInitialTerm();
            boolean match = term.match(currentParent.getRootValue(), currentNode.getRootValue());

            if (match) {
                LOG.debug("Found rule: " + rule);
                return rule;
            }
        }

        reportFinish("No rules matched");
        return null;
    }

    private void moveCursor(Command command) {
        String moveMessage = "Cursor moved ";
        String didntMoveMessage = "Cursor didn't move ";
        switch (command) {
            case UP:
                if (parentsStack.isEmpty()) {
                    LOG.debug(didntMoveMessage + command + ". Already at the top.");
                } else {
                    currentNode = currentParent;
                    currentParent = parentsStack.pop();
                    childrenPos = parentsStackPos.pop();

                    if (currentParent.getChildren().get(childrenPos) != currentNode) {
                        updateParent(false);
                    }

                    LOG.debug(moveMessage + command);
                }

                break;

            case DOWN:
                if (currentNode == ZERO_NODE) {
                    LOG.debug(didntMoveMessage + command + ". Standing on the leaf.");
                } else {
                    parentsStack.push(currentParent);
                    parentsStackPos.push(childrenPos);

                    currentParent = currentNode;

                    if (currentParent.hasChildren()) {
                        childrenPos = 0;
                        currentNode = currentParent.getChildren().get(childrenPos);
                        LOG.debug(moveMessage + command);
                    } else {
                        childrenPos = -1;
                        currentNode = ZERO_NODE;
                        LOG.debug(moveMessage + command + " to ZERO node.");
                    }
                }

                break;

            case LEFT:
                if (0 < childrenPos) {
                    childrenPos--;
                    currentNode = currentParent.getChildren().get(childrenPos);
                    LOG.debug(moveMessage + command);
                } else if (childrenPos == 0 && currentNode != ZERO_NODE) {
                    childrenPos--;
                    currentNode = ZERO_NODE;
                    LOG.debug(moveMessage + command + " to ZERO node.");
                } else {
                    LOG.debug(didntMoveMessage + command + ". Already at the left zero node.");
                }

                break;

            case RIGHT:
                if (childrenPos < currentParent.getChildren().size() - 1) {
                    childrenPos++;
                    currentNode = currentParent.getChildren().get(childrenPos);
                    LOG.debug(moveMessage + command);
                } else if (childrenPos == currentParent.getChildren().size() - 1 &&
                        currentNode != ZERO_NODE) {

                    childrenPos++;
                    currentNode = ZERO_NODE;
                    LOG.debug(moveMessage + command + " to ZERO node.");
                } else {
                    LOG.debug(didntMoveMessage + command + ". Already at the right zero node.");
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown command in move method.");
        }

        logAutomataStatus();
    }

    private void writeThisNode(String newLabel) {
        if (currentNode == ZERO_NODE) {
            if (childrenPos == -1) { childrenPos = 0; }

            currentNode = treeFactory.buildTree(newLabel);

            updateParent(true);

            LOG.debug("New node added");
            //logAutomataStatus();
        } else {
            currentNode = treeFactory.buildTree(newLabel, currentNode.getChildren());
            updateParent(false);

            LOG.debug("Write task performed");
            //logAutomataStatus();
        }
    }

    private void deleteCurrentNode(Command command) {
        if (currentNode == ZERO_NODE) {
            LOG.debug("Can't delete ZERO node.");
            return;
        }

        List<ITree> children = new ArrayList<>(currentParent.getChildren());
        switch (command) {
            case UP:
            case LEFT:
                children.remove(childrenPos);
                break;

            case RIGHT:
                childrenPos--;
                children.remove(childrenPos + 1);
                break;

            case DOWN:
                throw new IllegalArgumentException("Deletion with DOWN command is illegal rule");

            default:
                throw new IllegalArgumentException("Unknown command");
        }

        LOG.debug("Deleted " + currentNode);
        currentParent = treeFactory.buildTree(currentParent.getRootValue(), children);

        // we MUST move cursor to restore integrity of the automata!
        moveCursor(command);
    }

    private void updateParent(boolean add) {
        List<ITree> children = new ArrayList<>(currentParent.getChildren());

        if (add) {
            children.add(childrenPos, currentNode);
        } else {
            children.set(childrenPos, currentNode);
        }

        currentParent = treeFactory.buildTree(currentParent.getRootValue(), children);
    }

    private Map<Integer, List<AutomataRule>> buildStateMap(List<AutomataRule> rules) {
        Map<Integer, List<AutomataRule>> result = new HashMap<>();

        for (AutomataRule rule : rules) {
            List<AutomataRule> ruleList =
                    result.getOrDefault(rule.getInitialState(), new ArrayList<>());

            ruleList.add(rule);
            result.put(rule.getInitialState(), ruleList);
        }

        return result;
    }

    private void reportFinish(String message) {
        LOG.info("Automata stoped in state " + currentState);
        LOG.info("Reason: " + message);
        logAutomataStatus();
    }

    private void logAutomataStatus() {
        LOG.debug("Parent: " + currentParent.toString());
        LOG.debug("Child position: " + childrenPos);
        LOG.debug("Child: " + currentNode.toString());
        LOG.debug("State: " + currentState);
    }

    @Override
    public ITree getTree() { throw new UnsupportedOperationException(); }

    @Override
    public ITree getCurrentParent() {
        return currentParent;
    }

    @Override
    public ITree getCurrentNode() {
        return currentNode;
    }

    @Override
    public int getChildrenPos() {
        return childrenPos;
    }

    @Override
    public int getCurrentState() {
        return currentState;
    }

    @Override
    public AutomataRule getLastRule() {
        return lastRule;
    }
}
