package treegrammar.automata;

/**
 * Represents term for the automata rule
 */
public class Term {

    private final String parent;
    private final String child;

    Term(String parent, String child) {
        this.parent = parent;
        this.child = child;
    }

    public String getParent() {
        return parent;
    }

    public String getChild() {
        return child;
    }

    @Override
    public String toString() {
        return parent + "(" + (!child.equals("-") ? child : "") + ")";
    }

    public boolean match(String otherParent, String otherChild) {
        return parent.equals(otherParent) && child.equals(otherChild);
    }
}
