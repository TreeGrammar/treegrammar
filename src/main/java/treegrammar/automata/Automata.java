package treegrammar.automata;

import treegrammar.beans.ITree;

import java.util.List;

/**
 * Represents automata for grammatical trees.
 */
public interface Automata {

    /**
     * @return Current implementation of Automata.
     */
    static Automata getAutomata() {
        return new SingleTreeAutomataImpl();
    }

    /**
     * Process tree according to the rules.
     * @param tree Tree to process by this automata.
     * @param rules List of rules. See AutomataRule class for form details.
     */
    void process(ITree tree, List<AutomataRule> rules);

    void init(ITree tree, List<AutomataRule> rules);

    boolean nextStep();

    ITree getCurrentParent();

    ITree getCurrentNode();

    int getChildrenPos();

    int getCurrentState();

    AutomataRule getLastRule();

    ITree getTree();
}
