package treegrammar.automata;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents rule for automata
 */
public class AutomataRule {

    private int initialState;
    private Term initialTerm;

    private int nextState;
    private String substituteLable;
    private Command command;

    /**
     * Parse rure from its text representation.
     * @param textRule Text rule in form
     *                 'current_state, term -> next_state, new_label, move_direction
     * @return New AutomtatRule equivalent to provided text rule.
     */
    public static AutomataRule parseRule(String textRule) {

        String[] parts = textRule.split("\\s*->\\s*");
        String leftSide = parts[0];
        String rightSide = parts[1];

        String[] leftSideParts = leftSide.split("\\s*,\\s*");
        int initialState = Integer.parseInt(leftSideParts[0]);
        String termParent = leftSideParts[1].substring(0, 1);
        char childChar = leftSideParts[1].charAt(2);
        String termChild = childChar != ')' ? Character.toString(childChar) : "-";
        Term initialTerm = new Term(termParent, termChild);

        String[] rightSideParts = rightSide.split("\\s*,\\s*");
        int nextState;
        String substituteLable;
        Command command;
        if (rightSideParts[0].equals("#")) {
            nextState = Integer.MAX_VALUE;
            substituteLable = "#";
            command = Command.UP;
        } else {
            nextState = Integer.parseInt(rightSideParts[0]);
            substituteLable = rightSideParts[1];
            command = Command.fromString(rightSideParts[2]);
        }

        return new AutomataRule(initialState, initialTerm, nextState, substituteLable, command);
    }

    /**
     * Parse list of text rules.
     * @param textRules List of text rules for automata. See previous method for form details.
     * @return List of AutomataRule equivalent to list provided list.
     */
    public static List<AutomataRule> parseRules(List<String> textRules) {
        return textRules.stream().map(AutomataRule::parseRule).collect(Collectors.toList());
    }

    AutomataRule(int initialState, Term initialTerm, int nextState,
                        String substituteLable, Command command) {

        this.initialState = initialState;
        this.initialTerm = initialTerm;
        this.nextState = nextState;
        this.substituteLable = substituteLable;
        this.command = command;
    }

    public int getInitialState() {
        return initialState;
    }

    public Term getInitialTerm() {
        return initialTerm;
    }

    public int getNextState() {
        return nextState;
    }

    public String getSubstituteLabel() {
        return substituteLable;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public String toString() {
        // !!! Please, do not change this code, it is used for the back serialization of the rules to file
        return initialState + "," + initialTerm + " ->" + rightHand();
    }

    public void setInitialState(int initialState) {
        this.initialState = initialState;
    }

    public void setNextState(int nextState) {
        this.nextState = nextState;
    }

    public String rightHand() {
        return !substituteLable.equals("#") ?
                nextState + "," + substituteLable + "," + command.getText() :
                "#";
    }
}
