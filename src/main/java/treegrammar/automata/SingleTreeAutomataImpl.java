package treegrammar.automata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import treegrammar.beans.ITree;
import treegrammar.factories.TreeFactory;
import treegrammar.factories.TreeMutableFactory;
import treegrammar.beans.MutableTree;

import java.util.*;

/**
 * Implementation of automata.
 */
class SingleTreeAutomataImpl implements Automata {

    public static final String ZERO_LABEL = "-";
    private ITree tree;
    private ITree currentParent;
    private ITree currentNode;
    private int childrenPos;
    private int currentState;

    private static final TreeFactory treeFactory = TreeMutableFactory.getInstance();

    private Map<Integer, List<AutomataRule>> rulesByState;

    private static final Logger LOG = LoggerFactory.getLogger(SingleTreeAutomataImpl.class.getSimpleName());
    private AutomataRule lastRule;

    SingleTreeAutomataImpl() {
    }

    @Override
    public void process(ITree tree, List<AutomataRule> rules) {
        init(tree, rules);
        LOG.debug("----------------- Begin process -----------------------------");

        while (true) {
            if (nextStep()) return;
        }
    }

    @Override
    public void init(ITree tree, List<AutomataRule> rules) {
        rulesByState = buildStateMap(rules);

        // currentParent = treeFactory.buildTree("-", Collections.singletonList(tree));
        currentParent = tree;
        this.tree = currentParent;
        if (tree.getChildren().isEmpty()) {
            tree.getChildren().add(ITree.getEmptyTree());
        }
        currentNode = tree.getChildren().get(0);
        currentState = 0;
        childrenPos = 0;

        logAutomataStatus();
    }

    @Override
    public boolean nextStep() {
        lastRule = getNextRule();
        if (lastRule == null) {
            return true;
        }

        if (lastRule.getSubstituteLabel().equals("#")) {
            reportFinish("Reached final rule.");
            return true;
        }

        if (lastRule.getSubstituteLabel().equals(ZERO_LABEL)) {
            deleteCurrentNode(lastRule.getCommand());
            // we MUST move cursor to restore integrity of the automata!
            moveCursor(lastRule.getCommand());
        } else {
            // write new value only if it's different
            if (!lastRule.getSubstituteLabel().equals(currentNode.getRootValue())) {
                writeThisNode(lastRule.getSubstituteLabel());
            } else {
                LOG.debug("No write operation needed");
            }

            moveCursor(lastRule.getCommand());
        }

        currentState = lastRule.getNextState();
        logAutomataStatus();
        LOG.debug("----------------- Rule applied -----------------------------");
        return false;
    }

    private AutomataRule getNextRule() {
        List<AutomataRule> rulesForCurState = rulesByState.get(currentState);

        if (rulesForCurState == null || rulesForCurState.isEmpty()) {
            reportFinish("No rules for this state");
            return null;
        }

        for (AutomataRule rule : rulesForCurState) {
            Term term = rule.getInitialTerm();
            boolean match = term.match(currentParent.getRootValue(), currentNode.getRootValue());

            if (match) {
                LOG.debug("Found rule: " + rule);
                return rule;
            }
        }

        reportFinish("No rules matched");
        return null;
    }

    private void moveCursor(Command command) {
        String moveMessage = "Cursor moved ";
        String didntMoveMessage = "Cursor didn't move ";

        switch (command) {
            case UP:
                if (currentParent.getParent() == null) {
                    if (isZeroNode(currentParent) && isZeroNode(currentNode)) {
                        LOG.debug(didntMoveMessage + command + ". Already at the top.");
                    } else {
                        currentNode = currentParent;
                        currentParent = treeFactory.buildTree("-", Collections.singletonList(tree));
                        tree = currentParent;
                        childrenPos = 0;
                    }
                } else {
                    if (isZeroNode(currentNode)) {
                        currentParent.getChildren().remove(currentNode);
                    }

                    currentNode = currentParent;
                    currentParent = currentParent.getParent();
                    childrenPos = currentParent.getChildren().indexOf(currentNode);

                    LOG.debug(moveMessage + command);
                }

                break;

            case DOWN:
                if (isZeroNode(currentNode) && !isZeroNode(currentParent)) {
                    LOG.debug(didntMoveMessage + command + ". Standing on the leaf.");
                } else {
                    currentParent = currentNode;

                    // descending from the zero root
                    if (isZeroNode(currentParent.getParent())) {
                        ((MutableTree) currentParent).setParent(null);
                        tree = currentParent;
                    }

                    if (currentParent.hasChildren()) {
                        childrenPos = 0;
                        currentNode = currentParent.getChildren().get(childrenPos);
                        LOG.debug(moveMessage + command);
                    } else {
                        childrenPos = -1;
                        currentNode = buildZeroNode(childrenPos, currentParent);
                        LOG.debug(moveMessage + command + " to ZERO node.");
                    }
                }

                break;

            case LEFT:
                if (0 < childrenPos) {
                    if (isZeroNode(currentNode)) {
                        currentParent.getChildren().remove(currentNode);
                    }
                    childrenPos--;
                    currentNode = currentParent.getChildren().get(childrenPos);
                    LOG.debug(moveMessage + command);
                } else if (childrenPos == 0 && !isZeroNode(currentNode)) {
                    childrenPos--;
                    currentNode = buildZeroNode(childrenPos, currentParent);
                    LOG.debug(moveMessage + command + " to ZERO node.");
                } else {
                    LOG.debug(didntMoveMessage + command + ". Already at the left zero node.");
                }

                break;

            case RIGHT:
                if (childrenPos < currentParent.getChildren().size() - 1) {
                    if (isZeroNode(currentNode)) {
                        currentParent.getChildren().remove(currentNode);
                    }
                    childrenPos++;
                    currentNode = currentParent.getChildren().get(childrenPos);
                    LOG.debug(moveMessage + command);
                } else if (childrenPos == currentParent.getChildren().size() - 1 &&
                        !isZeroNode(currentNode)) {

                    childrenPos++;
                    currentNode = buildZeroNode(childrenPos, currentParent);
                    LOG.debug(moveMessage + command + " to ZERO node.");
                } else {
                    LOG.debug(didntMoveMessage + command + ". Already at the right zero node.");
                }
                break;

            default:
                throw new IllegalArgumentException("Unknown command in move method.");
        }

        //logAutomataStatus();
    }

    private ITree buildZeroNode(int childrenPos, ITree currentParent) {
        ITree newNode = treeFactory.buildTree(ZERO_LABEL);
        if (childrenPos >= 0) currentParent.getChildren().add(newNode);
        else currentParent.getChildren().add(0, newNode);
        // not nice, but fast decision
        ((MutableTree) newNode).setParent(currentParent);
        return newNode;
    }

    private boolean isZeroNode(ITree currentNode) {
        return ZERO_LABEL.equals(currentNode.label());
    }

    private void writeThisNode(String newLabel) {
        boolean wasZero = isZeroNode(currentNode);
        currentNode.setLabel(newLabel);
        if (wasZero) {
            if (childrenPos == -1) {
                childrenPos = 0;
            }
            LOG.debug("New node added");
            //logAutomataStatus();
        } else {
            LOG.debug("Write task performed");
            //logAutomataStatus();
        }
    }

    private void deleteCurrentNode(Command command) {
        if (isZeroNode(currentNode)) {
            return;
        }

        switch (command) {
            case UP:
            case LEFT:
                currentParent.getChildren().remove(childrenPos);
                break;

            case RIGHT:
                childrenPos--;
                currentParent.getChildren().remove(childrenPos + 1);
                break;

            case DOWN:
                LOG.debug("Deleted root of " + currentNode);
                if (currentNode.hasChildren()) {
                    currentNode = treeFactory.buildTree("-",
                            Collections.singletonList(currentNode.getChildren().get(0)));
                } else {
                    currentNode = treeFactory.buildTree("-");
                }

                currentParent = treeFactory.buildTree("-", Collections.singletonList(currentNode));
                tree = currentParent;
                currentNode.setLabel("-");
                return;

            default:
                throw new IllegalArgumentException("Unknown command");
        }

        LOG.debug("Deleted " + currentNode);
    }

    private Map<Integer, List<AutomataRule>> buildStateMap(List<AutomataRule> rules) {
        Map<Integer, List<AutomataRule>> result = new HashMap<>();

        for (AutomataRule rule : rules) {
            List<AutomataRule> ruleList =
                    result.getOrDefault(rule.getInitialState(), new ArrayList<>());

            ruleList.add(rule);
            result.put(rule.getInitialState(), ruleList);
        }

        return result;
    }

    private void reportFinish(String message) {
        LOG.info("Automata stoped in state " + currentState);
        LOG.info("Reason: " + message);
        logAutomataStatus();
    }

    private void logAutomataStatus() {
        LOG.debug("Parent: " + currentParent.toString());
        LOG.debug("Child position: " + childrenPos);
        LOG.debug("Child: " + currentNode.toString());
        LOG.debug("State: " + currentState);
    }

    @Override
    public ITree getTree() {
        return tree;
    }

    @Override
    public ITree getCurrentParent() {
        return currentParent;
    }

    @Override
    public ITree getCurrentNode() {
        return currentNode;
    }

    @Override
    public int getChildrenPos() {
        return childrenPos;
    }

    @Override
    public int getCurrentState() {
        return currentState;
    }

    @Override
    public AutomataRule getLastRule() {
        return lastRule;
    }
}
