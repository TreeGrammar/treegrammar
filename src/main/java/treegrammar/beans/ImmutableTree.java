package treegrammar.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Immutable recursive implementation of TreeGrammar.
 */
public class ImmutableTree implements ITree {

    protected final String value;
    protected final List<ITree> children;

    private int hash = 0;

    /**
     * Construct node of this tree.
     * @param value Non-empty single word string with value of this node.
     *              Should consist of uppercase and lowercase letters.
     * @param children Zero or more sub-trees. Order is matter and will be preserved.
     */
    public ImmutableTree(String value, List<ITree> children) {
        this.value = value;
        this.children = Collections.unmodifiableList(new ArrayList<>(children));
        //assert(!value.matches("[A-Za-z0-9]+"));
    }

    @Override
    public String getRootValue() {
        return value;
    }

    @Override
    public boolean hasChildren() {
        return !children.isEmpty();
    }

    @Override
    public List<ITree> getChildren() { return children; }

    @Override
    public boolean isTerminated() {
        if (hasChildren()) {
            for (ITree child : children) {
                if (!child.isTerminated()) {
                    return false;
                }
            }
        }

        return value.matches("[a-z]");
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(value);

        if (!(children.isEmpty())) {
            result.append('(');
            for (ITree child : children) {
                result.append(child.toString()).append(", ");
            }
            result.delete(result.length() - 2, result.length());
            result.append(')');
        }

        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || !(o instanceof ITree)) { return false; }

        ITree that = (ITree) o;

        if (!value.equals(that.getRootValue())) { return false; }

        List<ITree> thatChildren = that.getChildren();
        if (children.size() != thatChildren.size()) { return false; }

        for (int i = 0; i < children.size(); i++) {
            if (!children.get(i).equals(thatChildren.get(i))) { return false; }
        }

        return true;

    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            final int prime = 31;
            int result = value.hashCode();

            for (ITree child : children) {
                result = prime * result + child.hashCode();
            }

            hash = result;
        }

        return hash;
    }

    @Override
    public String label() {
        return value;
    }

}
