package treegrammar.beans;

import java.util.List;

/**
 * Represents immutable abstract ordered grammar tree.
 */

public interface ITree {

    /**
     * Build new variable with no children or any children.
     * @param value Value of this variable.
     * @param anyChildren True if variable contains any children,
     *                    false if this variable has not children.
     * @return Grammar tree with variable.
     */
    static ITree buildVar(String value, boolean anyChildren) {
        return new VariableTree(value, anyChildren);
    }

    /**
     * Build new variable with concrete list of children.
     * @param value Value of this variable.
     * @param children List of grammar trees considered direct children of this variable.
     * @return Grammar tree with variable.
     */
    static ITree buildVar(String value, List<ITree> children) {
        return new VariableTree(value, children);
    }

    static ITree getEmptyTree() {
        return EmptyTree.getEmptyTreeImp();
    }


    /**
     * Return value of the root node.
     * @return Non-empty string with value of the root node.
     */
    String getRootValue();

    /**
     * Check if this tree contains only one node.
     * @return true if root node has any children.
     */
    boolean hasChildren();

    /**
     * Return children of the root node.
     * @return Unmodifiable list of children in particular order.
     */
    List<ITree> getChildren();

    /**
     * Provide information if root value is variable.
     * @return true if root value matches 'X.+'
     */
    default boolean isRootVar() {
        return false;
    }

    /**
     * Provide info if this variable has no concrete children list, but assumes presence of children.
     * @return True if any list of children can be considered as list of children of this var.
     */
    default boolean varHasAnyChildren() {
        return false;
    }

    /**
     * Provide information if this tree consist only of terminals.
     * @return true if this tree consists only of terminals.
     */
    boolean isTerminated();

    String label();

    default ITree getParent() {
        return null;
    }

    default void setLabel(String newLabel) {
        throw new IllegalStateException("Not implemented");
    }
}
