package treegrammar.beans;

import java.util.Collections;
import java.util.List;

/**
 * This is singleton placeholder for using to describe index of "children" of leaf node.
 */
class EmptyTree implements ITree {
    private static final EmptyTree EMPTY_TREE = new EmptyTree();
    private static final String VALUE = "0";
    private final List<ITree> children = Collections.emptyList();

    // singleton
    private EmptyTree() { }

    /**
     * @return singleton of this class.
     */
    static EmptyTree getEmptyTreeImp() {
        return EMPTY_TREE;
    }

    @Override
    public String getRootValue() {
        return VALUE;
    }

    @Override
    public boolean hasChildren() {
        return false;
    }

    @Override
    public List<ITree> getChildren() {
        return children;
    }

    @Override
    public boolean isRootVar() {
        return false;
    }

    @Override
    public boolean varHasAnyChildren() {
        return false;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public String toString() {
        return VALUE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof ITree)) return false;

        ITree that = (ITree) o;

        return VALUE.equals(that.getRootValue()) && !that.hasChildren();

    }

    @Override
    public int hashCode() {
        int result = VALUE.hashCode();
        result = 31 * result + children.hashCode();
        return result;
    }

    @Override
    public String label() {
        return "";
    }

}
