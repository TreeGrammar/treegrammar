package treegrammar.beans;

import java.util.Collections;
import java.util.List;

/**
 * Class represents variable in grammar tree rule.
 */
class VariableTree extends ImmutableTree {
    private final boolean anyChildren;

    public VariableTree(String value, List<ITree> children) {
        super(value, children);
        anyChildren = false;
    }

    public VariableTree(String value, boolean anyChildren) {
        super(value, Collections.emptyList());
        this.anyChildren = anyChildren;
    }


    @Override
    public boolean hasChildren() {
        return super.hasChildren() || anyChildren;
    }

    @Override
    public boolean isRootVar() {
        return true;
    }

    @Override
    public boolean varHasAnyChildren() {
        return anyChildren;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public String toString() {
        if (hasChildren() || anyChildren) {
            return super.toString();
        }

        return getRootValue() + "()";
    }

    @Override
    public String label() {
        return "";
    }
}
