package treegrammar.beans;

import java.util.List;

/**
 * Created by andremoniy -
 * on 18/06/2017.
 */
public class MutableTree implements ITree {

    protected String value;
    protected List<ITree> children;
    private ITree parent;

    /**
     * Construct node of this tree.
     *
     * @param value    Non-empty single word string with value of this node.
     *                 Should consist of uppercase and lowercase letters.
     * @param children Zero or more sub-trees. Order is matter and will be preserved.
     */
    public MutableTree(String value, List<ITree> children) {
        this.value = value;
        this.children = children;

        for (ITree child : children) {
            ((MutableTree) child).parent = this;
        }
    }

    @Override
    public String getRootValue() {
        return value;
    }

    @Override
    public boolean hasChildren() {
        return children != null && !children.isEmpty();
    }

    @Override
    public List<ITree> getChildren() {
        return children;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof ITree)) {
            return false;
        }

        ITree that = (ITree) o;

        if (!value.equals(that.getRootValue())) {
            return false;
        }

        List<ITree> thatChildren = that.getChildren();
        if (children.size() != thatChildren.size()) {
            return false;
        }

        for (int i = 0; i < children.size(); i++) {
            if (!children.get(i).equals(thatChildren.get(i))) {
                return false;
            }
        }

        return true;

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = value.hashCode();

        for (ITree child : children) {
            result = prime * result + child.hashCode();
        }

        return result;
    }


    @Override
    public String label() {
        return value;
    }

    @Override
    public ITree getParent() {
        return parent;
    }

    @Override
    public void setLabel(String newLabel) {
        value = newLabel;
    }

    public void setParent(ITree parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(value);

        if (!(children.isEmpty())) {
            result.append('(');
            for (ITree child : children) {
                result.append(child.toString()).append(", ");
            }
            result.delete(result.length() - 2, result.length());
            result.append(')');
        }

        return result.toString();
    }
}
