package treegrammar.util;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Some static methods to read files
 */
public class GrammarFileUtils {

    public static List<String> readSource(String path) throws IOException {
        return IOUtils.readLines(GrammarFileUtils.class.getClassLoader().getResourceAsStream(path), "utf-8");
    }
}
