package treegrammar.search;

import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;

import java.util.List;

/**
 * Raw search results. Should be converted to SearchResult by appropriate consumer.
 */
class RawSearchResult {
    private final SearchNode current;
    private final ITree tree;
    private final ITree initialTree;
    private final List<GrammarRule> rules;
    private final int lastRule;

    RawSearchResult(SearchNode current, ITree tree, ITree initialTree,
                    List<GrammarRule> rules, int lastRule) {
        this.current = current;
        this.tree = tree;
        this.initialTree = initialTree;
        this.rules = rules;
        this.lastRule = lastRule;
    }

    SearchNode getCurrent() {
        return current;
    }

    ITree getTree() {
        return tree;
    }

    ITree getInitialTree() {
        return initialTree;
    }

    List<GrammarRule> getRules() {
        return rules;
    }

    int getLastRule() {
        return lastRule;
    }

    @Override
    public String toString() {
        return tree.toString();
    }
}
