package treegrammar.search;

import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Bean class to hold single founded resultTree.
 */
public class SearchResult {
    private ITree initialTree;
    private ITree resultTree;
    private List<GrammarRule> rules;

    SearchResult(ITree initialTree, ITree resultTree, List<GrammarRule> rules) {
        this.initialTree = initialTree;
        this.resultTree = resultTree;
        this.rules = Collections.unmodifiableList(new ArrayList<>(rules));
    }

    public ITree getInitialTree() {
        return initialTree;
    }

    public ITree getResultTree() {
        return resultTree;
    }

    public List<GrammarRule> getRules() {
        return rules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResult that = (SearchResult) o;

        return initialTree.equals(that.initialTree) &&
                resultTree.equals(that.resultTree) &&
                rules.equals(that.rules);

    }

    @Override
    public int hashCode() {
        int result = initialTree.hashCode();
        result = 31 * result + resultTree.hashCode();
        result = 31 * result + rules.hashCode();
        return result;
    }
}
