package treegrammar.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Start search in separate thread
 */
class Executor<SN, RRS, VT, RS> {
    private static final int VISITED_LIMIT = 100000;
    private static final long TIMEOUT = 1;
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;
    private static final int CONSUMER_QUEUE_CAPACITY = 100;
    private final Logger LOG = LoggerFactory.getLogger(this.getClass().getSimpleName());

    private final Searchable<SN, RRS, VT, RS> searcher;

    // if single-thread mode will prove useful move this enum to TreeSearch;
    // then mode and thread limit can be runtime defined as constructor property
    private enum MODE { SINGLE_THREAD, MULTI_THREAD }
    private final MODE mode = MODE.MULTI_THREAD;
    private final int threadLimit = 4;

    private Thread executorThread;
    private volatile boolean isDone = false;

    /**
     * Implementation of LRU cache replacement.
     * @param <K> Map key.
     * @param <V> Map value.
     */
    private static class LRU<K, V> extends LinkedHashMap<K, V> {
        private static final long serialVersionUID = 1L;
        private final int capacityLimit;

        private LRU(int capacity, float loadFactor) {
            super(capacity, loadFactor);
            this.capacityLimit = capacity;
        }
        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > capacityLimit;
        }
    }

    Executor(Searchable<SN, RRS, VT, RS> searcher) {
        this.searcher = searcher;
    }

    void start() {
        if (isDone) {
            throw new RuntimeException("Attempt to start executor which already finished its work");
        }
        executorThread = new Thread(this::execute);
        executorThread.start();
    }

    private void execute() {
        LOG.debug("Search started");
        // init
        BlockingQueue<SN> searchQueue;
        if (mode == MODE.SINGLE_THREAD) {
            searchQueue = new SingleThreadQueue<>();
        } else {
            searchQueue = new LinkedBlockingQueue<>();
        }

        BlockingQueue<RRS> rawSearchResults =
                new ArrayBlockingQueue<>(CONSUMER_QUEUE_CAPACITY);

        Set<VT> visited = Collections.newSetFromMap(
                new LRU<VT, Boolean>(VISITED_LIMIT, 0.7f));
        if (mode == MODE.MULTI_THREAD) {
            visited = Collections.synchronizedSet(visited);
        }

        List<RS> results = Collections.synchronizedList(new ArrayList<>());

        try {
            searcher.init(searchQueue, rawSearchResults, visited, results, TIMEOUT, TIME_UNIT);
        } catch (IllegalStateException e) {
            String errorMessage = "Search initialization failed.";
            LOG.error(errorMessage, e);
            throw new RuntimeException(e);
        }

        // start threads
        // consumer
        CountDownLatch doneSignalConsume = new CountDownLatch(1);
        Thread consumerThread = new Thread(() -> searcher.consume(doneSignalConsume));
        consumerThread.start();

        // producers
        int threadNum = (mode == MODE.SINGLE_THREAD) ? 1 : threadLimit;
        CountDownLatch doneSignalSearch = new CountDownLatch(threadNum);
        List<Thread> searchTreads = new ArrayList<>(threadNum);
        for (int i = 0; i < threadNum; i++) {
            Thread searchThread = new Thread(() -> searcher.search(doneSignalSearch));
            searchTreads.add(searchThread);
            searchThread.start();
        }
        LOG.debug("All threads commanded to start.");

        // wait until finished
        try {
            doneSignalSearch.await();
        } catch (InterruptedException e) {
            LOG.debug("Interrupt signal received. Shutting down.");
        }

        // shutting down threads if interrupted
        while (doneSignalSearch.getCount() > 0) {
            searchTreads.forEach(Thread::interrupt);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // do nothing
            }
        }

        // shutting down consumer
        consumerThread.interrupt();
        while (doneSignalConsume.getCount() > 0) {
            try {
                doneSignalConsume.await();
            } catch (InterruptedException e) {
                // do nothing
            }
        }

        LOG.debug("Search finished.");
        isDone = true;
    }

    void interrupt() {
        executorThread.interrupt();
    }

    boolean isDone() {
        return isDone;
    }
}
