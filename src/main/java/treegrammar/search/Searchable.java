package treegrammar.search;

import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Form of a task for executor.
 * WARNING! Search and consume methods MUST be thread-safe.
 * @param <SN> Search node which used as a start of search iteration.
 * @param <RRS> Raw result of search for consumer.
 * @param <VT> Class which will be put it visited cache.
 * @param <RS> Result of search for client.
 */
interface Searchable<SN, RRS, VT, RS> {

    /**
     * Ask searchable class to initialize queues.
     * @param searchQueue Queue which used to store intermediate results
     * @param rawResults Queue for outputting results.
     * @param visited Set of visited elements during this search.
     * @param timeout Time to wait for multi-threading queue operations.
     * @param timeUnit Time units for timeout.
     * @throws IllegalStateException If initialization went wrong.
     */
    void init(BlockingQueue<SN> searchQueue,
              BlockingQueue<RRS> rawResults,
              Set<VT> visited,
              List<RS> results,
              long timeout, TimeUnit timeUnit) throws IllegalStateException;

    /**
     * Perform search.
     * @param doneSignal Should be 'countedDown' when search is finished.
     */
    void search(CountDownLatch doneSignal);

    /**
     * Consume raw results produced by 'search' method.
     * If you don't need additional processing of them, just make RRS and RS same class
     * and in this method move results from queue to final list.
     * @param doneSignal Should be 'countedDown' when consumer is receive interrupt signal
     *                   and finish its job.
     */
    void consume(CountDownLatch doneSignal);

}
