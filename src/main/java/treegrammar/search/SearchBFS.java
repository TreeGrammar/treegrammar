package treegrammar.search;

import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;
import treegrammar.producer.Producer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple search (BFS).
 */
public class SearchBFS implements TreeSearch,
        Searchable<SearchNode, RawSearchResult, ITree, SearchResult> {

    private final static Logger LOG = LoggerFactory.getLogger(SearchBFS.class);

    private final int iterationsLimit;
    private final int recursionLimit;

    private ITree initialTree;
    private List<GrammarRule> rules;
    private Producer producer;
    private int iteration;

    private Executor<SearchNode, RawSearchResult, ITree, SearchResult> executor;
    private BlockingQueue<SearchNode> searchQueue;
    private BlockingQueue<RawSearchResult> rawResults;
    private Set<ITree> visited;
    private List<SearchResult> results;
    private long timeout;
    private TimeUnit timeUnit;

    /**
     * Produce instance of BFS search.
     * @param iterationsLimit Total number applied rules in a row.
     * @param recursionLimit Number of same rule applied in a row.
     * @return TreeSearch implemented as BFS search.
     */
    public static TreeSearch getSearchBFS(int iterationsLimit, int recursionLimit) {
        return new SearchBFS(iterationsLimit, recursionLimit);
    }

    // since this class implements some public interface methods which really should be
    // package private, this class should be instantiated throw fabric method.
    private SearchBFS(int iterationsLimit, int recursionLimit) {
        this.iterationsLimit = iterationsLimit;
        this.recursionLimit = recursionLimit;
    }

    @Override
    public void startSearch(ITree initialTree, List<GrammarRule> rules, Producer producer) {
        this.initialTree = initialTree;
        this.rules = rules;
        this.producer = producer;
        executor = new Executor<>(this);
        executor.start();
    }

    @Override
    public void interrupt() {
        executor.interrupt();
    }

    @Override
    public byte getProgress() {
        SearchNode node = searchQueue.peek();
        if (node != null && node.getIteration() > iteration) {
            iteration = node.getIteration();
        }

        //TODO: think some fancy math formula to produce more realistic progress.
        return (byte) (((double) iteration) / iterationsLimit * 100);
    }

    @Override
    public boolean isDone() {
        return executor.isDone();
    }

    @Override
    public List<SearchResult> getResult() {
        return Collections.unmodifiableList(results);
    }

    @Override
    public void init(BlockingQueue<SearchNode> searchQueue,
                     BlockingQueue<RawSearchResult> rawResults,
                     Set<ITree> visited,
                     List<SearchResult> results,
                     long timeout, TimeUnit timeUnit) {
        this.searchQueue = searchQueue;
        this.rawResults = rawResults;
        this.visited = visited;
        this.results = results;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        iteration = 0;

        byte[] backTrack = new byte[iterationsLimit + 1];
        Arrays.fill(backTrack, (byte) -1);

        try {
            boolean inserted =
                    searchQueue.offer(new SearchNode(initialTree, backTrack, 0), timeout, timeUnit);
            if (!inserted) { Thread.currentThread().interrupt(); }
        } catch (InterruptedException e) {
            String errorMessage = "Failed to initialize queue";
            LOG.error(errorMessage, e);
            throw new IllegalStateException(errorMessage, e);
        }

        visited.add(initialTree);
    }

    @Override
    public void search(CountDownLatch doneSignal) {

        LOG.debug("Starting BFS search.");

        boolean emptyQueue = false;
        while (!Thread.currentThread().isInterrupted()) {
            SearchNode current;
            try {
                current = searchQueue.poll(timeout, timeUnit);
                if (current == null) {
                    emptyQueue = true;
                    LOG.debug("Finished due to empty queue.");
                    break;
                }
            } catch (InterruptedException e) {
                break;
            }
            int iteration = current.getIteration() + 1;

            if (iteration > iterationsLimit) {
                //System.out.println("Terminated brunch: " + current.getResultTree());
                //stoppedBrunches.add(current.getResultTree());
                continue;
            }

            for (byte i = 0; i < rules.size(); i++) {
                GrammarRule rule = rules.get(i);
                ITree next = producer.produce(current.getTree(), rule);
                if (next.equals(current.getTree()) || visited.contains(next)) {
                    continue;
                }
                visited.add(next);

                if (next.isTerminated()) {
                    try {
                        boolean inserted = rawResults.offer(
                                new RawSearchResult(current, next, initialTree, rules, i),
                                timeout, timeUnit);
                        if (!inserted) { Thread.currentThread().interrupt(); }
                    } catch (InterruptedException e) {
                        String errorMessage = "Result consumer queue is full.";
                        LOG.error(errorMessage);
                        throw new RuntimeException(errorMessage);
                    }

                    continue;
                }

                //TODO: consider to introduce counter in search node
                byte[] passed = current.getPassed();
                if (passed[iteration - 1] == i) {
                    int rulesInRow = countSameRule(passed, iteration - 1);

                    if (rulesInRow >= recursionLimit) { continue; }
                }

                byte[] nextPassed = passed.clone();
                nextPassed[iteration] = i;
                searchQueue.add(new SearchNode(next, nextPassed, iteration));
            }
        }

        if (!emptyQueue) {
            LOG.debug("Finished due to interruption.");
        }
        doneSignal.countDown();
    }

    @Override
    public void consume(CountDownLatch doneSignal) {
        while (!Thread.interrupted()) {
            try {
                RawSearchResult rawResult = rawResults.take();
                LOG.info("Found terminated tree: " + rawResult);
                results.add(buildResult(rawResult));
            } catch (InterruptedException e) {
                break;
            }
        }

        LOG.debug("Consumer finished.");
        doneSignal.countDown();
    }

    /**
     * Count how many times same rule was already applied.
     * @param passed Backtrack array of rule numbers.
     * @param iteration Current iteration.
     * @return Number of time last rule was applied in a row.
     */
    private int countSameRule(byte[] passed, int iteration) {
        int count = 1;
        byte lastValue = passed[iteration--];
        for (int i = iteration; i > 0; i--) {
            if (passed[i] != lastValue)  { break; }
            count++;
        }

        return count;
    }

    /**
     * Build search result.
     * @param rawResult Raw search result.
     * @return Filled SearchResult.
     */
    private SearchResult buildResult(RawSearchResult rawResult) {
        byte[] passedRules = rawResult.getCurrent().getPassed();
        List<GrammarRule> pathRules = new ArrayList<>(passedRules.length);
        for (int i = 1; i < passedRules.length && passedRules[i] != -1; i++) {
            pathRules.add(rawResult.getRules().get(passedRules[i]));
        }
        pathRules.add(rawResult.getRules().get(rawResult.getLastRule()));

        return new SearchResult(rawResult.getInitialTree(), rawResult.getTree(), pathRules);
    }
}
