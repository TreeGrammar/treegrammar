package treegrammar.search;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * This is a wrapper for ArrayDeque for using it in a multi-threading environment.
 * THIS IS NOT A THREAD-SAFE QUEUE!
 * It just have necessary methods to comply with BlockingQueue interface.
 *
 * Main intended scenario:
 * we want to run multi-thread ready code with only one active thread
 * and beneficiate from ArrayDeque performance.
 */
class SingleThreadQueue<E> extends ArrayDeque<E> implements BlockingQueue<E> {
    // following two methods are meant to be used in single-thread search
    @Override
    public boolean offer(E e, long timeout, TimeUnit unit) {
        return super.offer(e);
    }

    @Override
    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        if (this.isEmpty()) throw new InterruptedException("SingleThreadQueue is empty");
        return super.poll();
    }

    // this is just an interface implementation
    @Override
    public E take() throws InterruptedException {
        return super.remove();
    }

    @Override
    public void put(E e) throws InterruptedException {
        super.offer(e);
    }

    @Override
    public int remainingCapacity() {
        return Integer.MAX_VALUE - super.size();
    }

    @Override
    public int drainTo(Collection<? super E> c) {
        int drained = this.size();
        c.addAll(this.stream().map(element -> this.poll()).collect(Collectors.toList()));
        return drained;
    }

    @Override
    public int drainTo(Collection<? super E> c, int maxElements) {
        throw new UnsupportedOperationException();
    }
}
