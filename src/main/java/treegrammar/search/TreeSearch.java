package treegrammar.search;

import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;
import treegrammar.producer.Producer;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Search for trees consisted only of terminals.
 * Search is launched in separate thread. After launching search throw startSearch method,
 * you can consider object of this interface as analog of java.util.concurrent.Future.
 */
public interface TreeSearch {

    Logger LOG = LoggerFactory.getLogger(TreeSearch.class);

    /**
     * Search for tree consisted only of terminals.
     * @param initialTree Initial tree for search
     * @param rules List of GrammarRules.
     * @param producer Producer to use in search
     */
    void startSearch(ITree initialTree, List<GrammarRule> rules, Producer producer);

    /**
     * Abort the search process. Doesn't affect on already yielded results.
     * After this method actual shutting down can take some time to finish current round of search.
     */
    void interrupt();

    /**
     * Return estimated progress in percents.
     * @return Progress value in range 0-100.
     */
    byte getProgress();

    /**
     * Provide information if all operations during the search was finished or interrupted.
     * @return true if all search results are ready to consume and no new results will be added.
     */
    boolean isDone();

    /**
     * Give a list with search results.
     * List can contain partial result if search is still in process.
     * @return Immutable list of SearchResults.
     */
    List<SearchResult> getResult();

    //TODO: remove printing from these methods! They should only provide data structures with results.
    /**
     * Print chain of trees from initial tree to found tree.
     * @param result Result of the search.
     */
    static void showResult(SearchResult result, Producer producer) {
        LOG.info(result.getInitialTree() + " =>");
        ITree currentTree = result.getInitialTree();
        List<GrammarRule> rules = result.getRules();
        for (GrammarRule rule : rules) {
            int ruleNum = rule.getRuleNum();
            currentTree = producer.produce(currentTree, rule);
            String msg = currentTree.toString() + " - [rule " + ruleNum + ": " + rule + "]";

            LOG.info(msg);
        }
    }

    /**
     * Print number of usage for each rule.
     * @param result Result of the search.
     */
    static void showRuleStat(SearchResult result) {
        List<GrammarRule> rules = result.getRules();

        TreeMap<GrammarRule, Integer> ruleCount = new TreeMap<>();

        int maxLength = 0;
        for (GrammarRule rule : rules) {
            maxLength = Math.max(maxLength, rule.toString().length());
            if (ruleCount.containsKey(rule)) {
                ruleCount.put(rule, ruleCount.get(rule) + 1);
            } else {
                ruleCount.put(rule, 1);
            }
        }

        LOG.info("Rule frequency stat:");
        for (Map.Entry<GrammarRule, Integer> entry : ruleCount.entrySet()) {
            double percent = entry.getValue() / (double) rules.size() * 100;
            LOG.info(String.format("%2s. [%-" + maxLength + "s] - %d (%.2f%%)",
                    entry.getKey().getRuleNum(), entry.getKey(), entry.getValue(), percent));
        }

        LOG.info("Total: " + rules.size());
        LOG.info("------------------------");

    }

    /**
     * Print total usage for each rule during the search.
     * @param rules List of used rules.
     * @param results Lost of found results.
     */
    static void showTotalStat(List<GrammarRule> rules, List<SearchResult> results) {
        TreeMap<GrammarRule, Integer> ruleCount = new TreeMap<>();

        // search for longest rule string
        int maxLength = 0;
        for (GrammarRule rule : rules) {
            maxLength = Math.max(maxLength, rule.toString().length());
        }

        // count rule usage
        int totalRuleUsage = 0;
        int maxUsage = 0;
        for (SearchResult result : results) {
            List<GrammarRule> resultRules = result.getRules();

            for (GrammarRule rule : resultRules) {
                int usage = 1;
                if (ruleCount.containsKey(rule)) {
                    usage = ruleCount.get(rule) + 1;
                    ruleCount.put(rule, usage);
                } else {
                    ruleCount.put(rule, usage);
                }
                maxUsage = Math.max(maxUsage, usage);
                totalRuleUsage++;
            }
        }

        // display info
        LOG.info("Total rules frequency stat:");
        for (int i = 0; i < rules.size(); i++) {
            GrammarRule rule;
            int count = 0;
            if (!ruleCount.isEmpty() &&
                    ruleCount.firstEntry().getKey().getRuleNum() == i + 1) {
                rule = ruleCount.firstKey();
                count = ruleCount.remove(rule);
            } else {
                rule = rules.get(i);
            }

            double percent = count / (double) totalRuleUsage * 100;
            LOG.info(String.format("%2s. [%-" + maxLength + "s] - %-" +
                            String.valueOf(maxUsage).length() + "d (%.2f%%)",
                    rule.getRuleNum(), rule, count, percent));
        }
        LOG.info("Total: " + totalRuleUsage);
    }
}
