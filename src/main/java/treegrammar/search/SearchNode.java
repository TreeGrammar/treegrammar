package treegrammar.search;

import treegrammar.beans.ITree;

/**
 * Represent complete set of information to perform next round of search.
 */
class SearchNode {
    private final ITree tree;
    private final int iteration;
    private final byte[] passed;

    SearchNode(ITree tree, byte[] passed, int iteration) {
        this.tree = tree;
        this.passed = passed;
        this.iteration = iteration;
    }

    ITree getTree() {
        return tree;
    }

    int getIteration() {
        return iteration;
    }

    byte[] getPassed() {
        return passed;
    }

    @Override
    public String toString() {
        return tree.toString();
    }
}
