package treegrammar.producer;

import treegrammar.beans.ITree;
import treegrammar.factories.TreeFactory;
import treegrammar.factories.TreeImmutableFactory;
import treegrammar.grammar.*;
import treegrammar.matcher.Matcher;
import treegrammar.matcher.MatcherResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of Producer.
 */
class ProducerImp implements Producer {
    private final Mode mode;

    private final static TreeFactory treeFactory = TreeImmutableFactory.getInstance();

    ProducerImp(Mode mode) {
        this.mode = mode;
    }

    @Override
    public ITree produce(ITree tree, GrammarRule rule) {
        Matcher matcher = Matcher.getDefaultMatcher();

        MatcherResult result = matcher.match(tree, rule.getPattern());
        if (result == null) { return tree; }

        ITree newSubTree =
                buildSubTree(result.getMatch(), result.getPatternIndex(),
                        result.getVariables(), rule.getTransformation(), true);

        return substitute(tree, result.getMatch(), newSubTree);
    }

    /**
     * Recursively build sub-tree from old sub-tree and info provided by Matcher.
     * @param oldTree Old sub-tree. Will be used to copy children not matched by patter.
     * @param indexes Positions of matched elements in children list.
     * @param vars Map with variables values.
     * @param blueprint Template of new sub-tree.
     * @return New sub-tree according to template and old sub-tree.
     */
    private ITree buildSubTree(ITree oldTree,
                                     IndexTree indexes, Map<String, ITree> vars,
                                     ITree blueprint, boolean transferChildren) {

        // substitute blueprint with actual value of variable
        if (blueprint.isRootVar() && blueprint.varHasAnyChildren()) {
            blueprint = vars.get(blueprint.getRootValue());
        }

        String rootValue = getValue(blueprint, vars);

        List<ITree> oldChildren = oldTree.getChildren();
        int patternIndex = indexes.getRootValue();
        int afterChildrenIdx = patternIndex + indexes.getChildrenSize();
        int newChildrenSize = patternIndex + blueprint.getChildren().size() +
                oldChildren.size() - afterChildrenIdx;

        List<ITree> newChildren = new ArrayList<>(newChildrenSize);
        if (transferChildren) {
            newChildren.addAll(oldChildren.subList(0, patternIndex));
        }

        boolean transfer = true;
        if (mode == Mode.STRICT) { transfer = newChildrenSize == oldChildren.size(); }

        if (blueprint.hasChildren()) {
            List<ITree> blueprintChildren = blueprint.getChildren();
            int startPosIfRight = blueprintChildren.size() - indexes.getChildrenSize();

            for (int i = 0; i < blueprintChildren.size(); i++) {
                int posInOldTree = patternIndex + i;

                // empty stubs if element is structurally new
                ITree oldChild = ITree.getEmptyTree();
                IndexTree subIndexes = IndexTree.getEmptyTree();

                // if we in a matched segment, provide useful info
                if ((mode == Mode.LEFT || mode == Mode.STRICT)
                        && posInOldTree < afterChildrenIdx) {
                    oldChild = oldChildren.get(posInOldTree);
                    subIndexes = indexes.getChild(i);
                } else if (mode == Mode.RIGHT && i >= startPosIfRight) {
                    oldChild = oldChildren.get(posInOldTree - startPosIfRight);
                    subIndexes = indexes.getChild(i - startPosIfRight);
                }

                ITree blueprintChild = blueprintChildren.get(i);

                ITree child = buildSubTree(
                        oldChild, subIndexes, vars, blueprintChild, transfer);
                newChildren.add(child);
            }
        }

        if (transferChildren) {
            newChildren.addAll(oldChildren.subList(afterChildrenIdx, oldChildren.size()));
        }

        if (newChildren.isEmpty())
            return treeFactory.buildTree(rootValue);
        else {
            return treeFactory.buildTree(rootValue, newChildren);
        }
    }

    /**
     * Substitute one sub-tree with another.
     * Since TreeGrammar is immutable it is guaranteed that old tree will stay untouched.
     * If tree doesn't contain old sub-tree(same object!), same tree will be returned.
     * @param tree Old tree.
     * @param oldSubTree Subtree which will be substituted.
     * @param newSubTree Subtree to substitute.
     * @return Tree with substituted sub-tree if old sub-tree was found. Otherwise same tree.
     */
    private ITree substitute(ITree tree, ITree oldSubTree, ITree newSubTree) {
        // consider to use equals, but should thought throw consequences first
        if (tree == oldSubTree) { return newSubTree; }
        if (!tree.hasChildren()) { return tree; }

        List<ITree> oldChildren = tree.getChildren();
        List<ITree> newChildren = new ArrayList<>(tree.getChildren().size());
        for (ITree child : oldChildren) {
            ITree newChild = substitute(child, oldSubTree, newSubTree);
            newChildren.add(newChild);
        }

        return treeFactory.buildTree(tree.getRootValue(), newChildren);
    }

    /**
     * Provide value for root.
     * @param transform Template with value or variable in root node.
     * @param vars Map with variable values.
     * @return Value of root node of template, or value of variable.
     */
    private String getValue(ITree transform, Map<String, ITree> vars) {
        if (vars.containsKey(transform.getRootValue())) {
            return vars.get(transform.getRootValue()).getRootValue();
        }

        return transform.getRootValue();
    }
}
