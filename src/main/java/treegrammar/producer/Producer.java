package treegrammar.producer;

import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;

/**
 * Produce a new TreeGrammar from initial tree and grammar rule:
 * Producer(tree:TreeGrammar, rule:TreeGrammar) -> TreeGrammar
 */
public interface Producer {
    enum Mode { LEFT, RIGHT, STRICT }

    static Producer getDefaultProducer() {
        return new ProducerImp(Mode.LEFT);
    }

    static Producer getDefaultProducer(Mode mode) {
        return new ProducerImp(mode);
    }

    /**
     * Produce a new TreeGrammar from initial tree and GrammarRule.
     * @param tree Initial tree.
     * @param rule Rule to apply.
     * @return New TreeGrammar or
     *         initial TreeGrammar, if initial tree doesn't match pattern from the rule.
     */
    ITree produce(ITree tree, GrammarRule rule);
}
