package treegrammar.producer;

import treegrammar.grammar.GrammarParser;
import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Tests for Producer
 */
@RunWith(Parameterized.class)
public class ProducerTest {

    @Parameterized.Parameters(name="{0}")
    public static Iterable<Object[]> expressions() {
        return Arrays.asList(new Object[][] {
            // LEFT
            { "Test1", "LEFT", "A(a, b(f, e), c)", "A(X1(f)) -> A(b, d)", "A(a, b(e), d, c)" },
            { "Ex1_1", "LEFT", "St", "St -> s(D,Tt,A)", "s(D,Tt,A)" },
            { "Ex1_2", "LEFT", "s(D,Tt,A)", "Tt -> t(D,Tt,a)", "s(D,t(D,Tt,a),A)" },
            { "Ex1_3", "LEFT", "s(D,t(D,Tt,a),A)", "Tt -> t(D,a)", "s(D,t(D,t(D,a),a),A)" },
            { "Ex1_4", "LEFT", "s(D,t(D,t(D,a),a),A)", "X1(D,a) -> X1(a,b,D)", "s(D,t(D,t(a,b,D),a),A)" },
            { "Ex1_5", "LEFT", "s(D,t(D,t(a,b,D),a),A)", "X1(D,X2(a)) -> X1(a,X2(b,D))", "s(D,t(a,t(b,D,b,D),a),A)" },
            { "Ex1_6", "LEFT", "s(a,t(b,b,t(b,D,D,D),a),A)", "X1(X2(D),a) -> X1(X2(a),b,D)", "s(a,t(b,b,t(b,D,D,a),b,D),A)" },
            { "Ex1_7", "LEFT", "s(a,t(b,D,t(b,D,b,D),a),A)", "X1(D,X2(b)) -> X1(b,X2(D))", "s(a,t(b,b,t(D,D,b,D),a),A)" },
            { "Ex1_8", "LEFT", "s(a,t(b,b,t(b,a,b,D,b,D),b,D),A)", "X1(D,b) -> X1(b,D)", "s(a,t(b,b,t(b,a,b,b,D,D),b,D),A)" },
            { "Ex1_9", "LEFT", "s(a,t(b,b,t(b,a,b,b,D,D),b,D),A)", "X1(X2(D),b) -> X1(X2(b),D)", "s(a,t(b,b,t(b,a,b,b,D,b),D,D),A)" },
            { "Ex1_10", "LEFT", "s(a,t(b,b,t(b,a,b,b,b,D),D,D),A)", "X1(X2(D),A) -> X1(X2(A),b)", "s(a,t(b,b,t(b,a,b,b,b,D),D,A),b)" },
            { "Ex1_11", "LEFT", "s(a,t(b,b,t(b,a,b,b,b,D),D,A),b)", "X1(D,A) -> X1(A,b)", "s(a,t(b,b,t(b,a,b,b,b,D),A,b),b)" },
            { "Ex1_12", "LEFT", "s(a,t(b,b,t(b,a,b,b,b,A),b,b),b)", "A -> a", "s(a,t(b,b,t(b,a,b,b,b,a),b,b),b)" },
            { "DelTree_1", "LEFT", "s(a, b)", "X1(a, b) -> X1()", "s"},
            { "DelTree_2", "LEFT", "s(a,A,B,c)", "X1(A, B) -> X1()", "s(a, c)"},
            { "DelTree_3", "LEFT", "s(a, b(a, A, B, c), c)", "X1(X2(A, B)) -> X1(X2())", "s(a, b(a, c), c)"},
            { "VarPointsTree_1", "LEFT", "A(y, a(x, b, a, d(b, c), f(e, g), x), z)",
                    "X1(X2(), a, X3(b, c), X4) -> C(X1(a, X2(), X3(c, b), X4))",
                    "A(y, C(x, a(a, b, d(c, b), f(e, g)), x), z)"},
            { "VarPointsTree_2", "LEFT", "C(b(a, a))", "C(X1) -> X1", "b(a, a)"},
            { "VarPointsTree_3", "LEFT", "A(y, a(x, b, a, d(b, c), f(e, g), x), z)",
                    "A(X1(X2(), a, X3(b, c), X4)) -> X4", "f(y, e(x, x), g, z)" },
            { "VarPointsTree_4", "LEFT", "A(y, a(h, b, d(b), f(e, g), j), z)",
                    "X1(a(b, X2, X3)) -> X3", "f(y, e(h, j), g, z)"},
            // STRICT
            { "ST_Test1", "STRICT", "A(a, b(f, e), c)", "A(X1(f)) -> A(b, d)", "A(a, b, d, c)" },
            { "ST_VarPointsTree_1", "STRICT", "A(y, a(h, b, d(b), f(e, g), j), z)",
                    "X1(a(b, X2, X3)) -> X3", "f(y, e, g, z)"},
            { "ST_VarPointsTree_2", "STRICT", "A(y, a(h, b, d(b), f(e, g), j), z)",
                    "X1(a(b, X2, X3)) -> X2", "d(y, b(h, j), z)"},
            { "ST_Ex1_7", "STRICT", "s(a, t(b, b, t(D, D, b, D), a), A)",
                    "X1(X2(D), a) -> X1(X2(a), b, D)", "s(a, t(b, b, t(a), b, D), A)" },
            //RIGHT
            { "RT_Test1", "RIGHT", "A(a, b(f, e), c)", "A(X1(f)) -> A(b, d)", "A(a, b, d(e), c)" },
            { "RT_VarPointsTree_1", "RIGHT", "A(y, a(h, b, d(b), f(e, g), j), z)",
                    "X1(a(b, X2, X3)) -> X3", "f(y, e, g(h, j), z)"},
            { "RT_VarPointsTree_2", "RIGHT", "A(y, a(x, b, a, d(b, c), f(e, g), x), z)",
                    "A(X1(X2(), a, X3(b, c), X4)) -> X4","f(y, e, g(x, x), z)" },
            { "RT_Ex1_7", "RIGHT", "s(a, t(b, b, t(D, D, b, D), a), A)",
                    "X1(X2(D), a) -> X1(X2(a), b, D)", "s(a, t(b, b, t(a), b(D, D, b), D), A)" },
        });
    }

    private final String name;
    private final Producer.Mode mode;
    private final ITree tree;
    private final GrammarRule rule;
    private final ITree expected;

    public ProducerTest(String name, String mode, String treeStr, String ruleStr, String newTreeStr) {
        GrammarParser parser = GrammarParser.getParser();
        this.name = name;
        this.mode = Producer.Mode.valueOf(mode);
        this.tree = parser.parseTree(treeStr);
        this.rule = parser.parseRules(ruleStr).get(0);
        this.expected = parser.parseTree(newTreeStr);
    }

    @Test
    public void produce() throws Exception {
        Producer producer = Producer.getDefaultProducer(mode);
        ITree actual = producer.produce(tree, rule);

        assertThat(name, actual, is(equalTo(expected)));
    }
}