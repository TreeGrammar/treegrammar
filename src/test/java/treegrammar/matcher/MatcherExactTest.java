package treegrammar.matcher;

import treegrammar.grammar.GrammarParser;
import treegrammar.beans.ITree;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test cases for MatcherExact
 */
public class MatcherExactTest {
    private final MatcherExact MATCHER = new MatcherExact();
    private final GrammarParser PARSER = GrammarParser.getParser();
    private final ITree TREE = PARSER.parseTree("A(b, c(d, e))");
    private final ITree INDEX_TREE = PARSER.parseTree("0(0, 0(0, 0))");

//    @Test
//    public void match_StrictNoVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "A(b, c(d, e)) -> A(A, B)"
//        );
//
//        MatcherResult expected = new MatcherResult(TREE, Collections.emptyMap(), INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.STRICT);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_StrictWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, c(d, X2)) -> b(A, B)"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X2", "e");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.STRICT);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_StrictNotMatched() throws Exception {
//        GrammarParser parser = GrammarParser.getParser();
//
//        TreeGrammar tree = parser.parseTree("A(b, c(d, e))");
//
//        List<GrammarRule> rules = parser.parseRules(
//                "A(a, c(d, e)) -> a",
//                "X1(X2, X3(d)) -> c",
//                "X1(X2, a, c(d, e)) -> b"
//        );
//
//        MatcherResult expected = null;
//
//        for (GrammarRule rule : rules) {
//            MatcherResult actual = MATCHER.match(
//                    tree, rule.getPattern(), MatcherExact.Mode.STRICT);
//
//            //noinspection ConstantConditions
//            assertThat(actual, is(equalTo(expected)));
//        }
//    }
//
//    @Test
//    public void match_RightBorderWholeTreeNoVar() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "A(b, c(d, e)) -> A(A, B)"
//        );
//
//        MatcherResult expected = new MatcherResult(TREE, Collections.emptyMap(), INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.RIGHT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_RightBorderWholeTreeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, c(d, X2)) -> b(A, B)"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X2", "e");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.RIGHT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_RightBorderPartTreeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, X3(d)) -> c"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X3", "c");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0(0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.RIGHT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_RightBorderOneSubNodeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b) -> b"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0)");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.RIGHT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_RightBorderNotMatched() throws Exception {
//        GrammarParser parser = GrammarParser.getParser();
//
//        TreeGrammar tree = parser.parseTree("A(b, c(d, e))");
//
//        List<GrammarRule> rules = parser.parseRules(
//                "X1(c(d, e)) -> b",
//                "A(a, c(d, e)) -> a",
//                "X1(b, c(e)) -> b"
//        );
//
//        MatcherResult expected = null;
//
//        for (GrammarRule rule : rules) {
//            MatcherResult actual = MATCHER.match(
//                    tree, rule.getPattern(), MatcherExact.Mode.RIGHT_BORDER);
//
//            //noinspection ConstantConditions
//            assertThat(actual, is(equalTo(expected)));
//        }
//    }
//
//    @Test
//    public void match_LeftBorderWholeTreeNoVar() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "A(b, c(d, e)) -> A(A, B)"
//        );
//
//        MatcherResult expected = new MatcherResult(TREE, Collections.emptyMap(), INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.LEFT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_LeftBorderWholeTreeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, c(d, X2)) -> b(A, B)"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X2", "e");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, INDEX_TREE);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.LEFT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_LeftBorderPartTreeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(c(X4, e)) -> b"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X4", "d");
//
//        TreeGrammar indexTree = PARSER.parseTree("1(0(0, 0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.LEFT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_LeftBorderOneSubNodeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(c(e)) -> b"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//
//        TreeGrammar indexTree = PARSER.parseTree("1(1(0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(
//                TREE, rules.get(0).getPattern(), MatcherExact.Mode.LEFT_BORDER);
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_LeftBorderNotMatched() throws Exception {
//        GrammarParser parser = GrammarParser.getParser();
//
//        TreeGrammar tree = parser.parseTree("A(b, c(d, e))");
//
//        List<GrammarRule> rules = parser.parseRules(
//                "X1(X2, X3(d)) -> c",
//                "A(a, c(d, e)) -> a",
//                "X1(b, c(e)) -> b"
//        );
//
//        MatcherResult expected = null;
//
//        for (GrammarRule rule : rules) {
//            MatcherResult actual = MATCHER.match(
//                    tree, rule.getPattern(), MatcherExact.Mode.LEFT_BORDER);
//
//            //noinspection ConstantConditions
//            assertThat(actual, is(equalTo(expected)));
//        }
//    }
}
