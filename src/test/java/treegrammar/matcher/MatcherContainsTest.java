package treegrammar.matcher;

import treegrammar.grammar.GrammarParser;
import treegrammar.grammar.GrammarRule;
import treegrammar.beans.ITree;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MatcherContainsTest {
    private final Matcher MATCHER = Matcher.getDefaultMatcher();
    private final GrammarParser PARSER = GrammarParser.getParser();
    private final ITree TREE = PARSER.parseTree("A(b, c(d, e))");

    @Test
    public void match_MyWrongNodes() throws Exception {
        List<GrammarRule> rules = PARSER.parseRules(
                "A(a, c(d, e)) -> a"
        );

        MatcherResult expected = null;
        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());

        //noinspection ConstantConditions
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void match_MySplitRange() throws Exception {
        List<GrammarRule> rules = PARSER.parseRules(
                "X1(b, c(e)) -> b"
        );

        MatcherResult expected = null;
        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());

        //noinspection ConstantConditions
        assertThat(actual, is(equalTo(expected)));
    }

//    @Test
//    public void match_MyWholeTreeNoVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "A(b, c(d, e)) -> A(A, B)"
//        );
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0(0, 0))");
//
//        MatcherResult expected = new MatcherResult(TREE, Collections.emptyMap(), indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MyWholeTreeWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, c(d, X2)) -> b(A, B)"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X2", "e");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0(0, 0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MyRightBorderWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b, X3(d)) -> c"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//        vars.put("X3", "c");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0(0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MyLeftBorderWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(c(d, e)) -> b"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//
//        TreeGrammar indexTree = PARSER.parseTree("1(0(0, 0))");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MyRightBorderOneChildWithVars() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(b) -> b"
//        );
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "A");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0)");
//
//        MatcherResult expected = new MatcherResult(TREE, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MySubTreeOneChildRightBorder() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(d) -> b"
//        );
//
//        TreeGrammar tree = PARSER.parseTree("c(d, e)");
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "c");
//
//        TreeGrammar indexTree = PARSER.parseTree("0(0)");
//
//        MatcherResult expected = new MatcherResult(tree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MySubTreeOneChildLeftBorder() throws Exception {
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(e) -> b"
//        );
//
//        TreeGrammar tree = PARSER.parseTree("c(d, e)");
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "c");
//
//        TreeGrammar indexTree = PARSER.parseTree("1(0)");
//
//        MatcherResult expected = new MatcherResult(tree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(TREE, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_MySubTreeOneChildMiddle() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("A(b, c(d, e, f))");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(e) -> b"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("c(d, e, f)");
//
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "c");
//
//        TreeGrammar indexTree = PARSER.parseTree("1(0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//
//    @Test
//    public void match_Example1_1() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("St");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "St -> s(D,Tt,A)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("St");
//        TreeGrammar indexTree = PARSER.parseTree("0");
//
//        MatcherResult expected = new MatcherResult(expectedTree, Collections.emptyMap(), indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_2() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(D,Tt,A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "Tt -> t(D,Tt,a)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("Tt");
//        TreeGrammar indexTree = PARSER.parseTree("0");
//
//        MatcherResult expected = new MatcherResult(expectedTree, Collections.emptyMap(), indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_3() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(D,t(D,Tt,a),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "Tt -> t(D,a)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("Tt");
//        TreeGrammar indexTree = PARSER.parseTree("0");
//
//        MatcherResult expected = new MatcherResult(expectedTree, Collections.emptyMap(), indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_4() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(D,t(D,t(D,a),a),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(D,a) -> X1(a,b,D)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(D,a)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_5() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(D,t(D,t(a,b,D),a),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(D,X2(a)) -> X1(a,X2(b,D))"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(D,t(a,b,D),a)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        vars.put("X2", "t");
//        TreeGrammar indexTree = PARSER.parseTree("0(0, 0(0))");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_6() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,D,D,D),a),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(X2(D),a) -> X1(X2(a),b,D)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,b,t(b,D,D,D),a)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        vars.put("X2", "t");
//        TreeGrammar indexTree = PARSER.parseTree("2(3(0), 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_7() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,D,t(b,D,b,D),a),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(D,X2(b)) -> X1(b,X2(D))"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,D,t(b,D,b,D),a)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        vars.put("X2", "t");
//        TreeGrammar indexTree = PARSER.parseTree("1(0, 0(0))");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_8() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,a,b,b,D,b),D,D),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(D,b) -> X1(b,D)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,a,b,b,D,b)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        TreeGrammar indexTree = PARSER.parseTree("4(0, 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_9() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,a,b,b,D,D),b,D),A)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(X2(D),b) -> X1(X2(b),D)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,b,t(b,a,b,b,D,D),b,D)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        vars.put("X2", "t");
//        TreeGrammar indexTree = PARSER.parseTree("2(5(0), 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_10() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,a,b,b,b,D),A,b),b)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(X2(D),A) -> X1(X2(A),b)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,b,t(b,a,b,b,b,D),A,b)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        vars.put("X2", "t");
//        TreeGrammar indexTree = PARSER.parseTree("2(5(0), 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_11() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,a,b,b,b,D),D,A),b)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "X1(D,A) -> X1(A,b)"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("t(b,b,t(b,a,b,b,b,D),D,A)");
//        Map<String, String> vars = new HashMap<>();
//        vars.put("X1", "t");
//        TreeGrammar indexTree = PARSER.parseTree("3(0, 0)");
//
//        MatcherResult expected = new MatcherResult(expectedTree, vars, indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
//
//    @Test
//    public void match_Example1_12() throws Exception {
//        TreeGrammar tree = PARSER.parseTree("s(a,t(b,b,t(b,a,b,b,b,A),b,b),b)");
//        List<GrammarRule> rules = PARSER.parseRules(
//                "A -> a"
//        );
//
//        TreeGrammar expectedTree = PARSER.parseTree("A");
//        TreeGrammar indexTree = PARSER.parseTree("0");
//
//        MatcherResult expected = new MatcherResult(expectedTree, Collections.emptyMap(), indexTree);
//        MatcherResult actual = MATCHER.match(tree, rules.get(0).getPattern());
//        assertThat(actual, is(equalTo(expected)));
//    }
}
